#!/usr/bin/perl -w

use strict;
use warnings;

use local::lib;
use lib( $ENV{'WIREBIRDWD'} || $ENV{'PWD'} )
  . "/lib";    # use local-directory libraries first

use Test::More;
    eval "use Test::Pod::Coverage";

my $trustme =
      { trustme =>
          [qr/^(handlers|pagetypes)$/] };

all_pod_coverage_ok( $trustme );
