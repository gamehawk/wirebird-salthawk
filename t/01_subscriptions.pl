#!/usr/bin/perl

use strict;
use Test::More;
    eval "use Test::Pod::Coverage";
binmode(STDOUT, ":utf8");

use local::lib;
use lib( $ENV{'WIREBIRDWD'} || $ENV{'PWD'} )
  . "/lib";    # use local-directory libraries first
use Wirebird;
use Wirebird::Resource;
use Wirebird::Handler::Profile;
use RDF::Trine::Store::Memory;
use RDF::TrineX::Functions -all;

BAIL_OUT('Could not initialize Wirebird') unless my $wirebird = Wirebird->new;
my $username = getlogin || getpwuid( $< ) || $ENV{ LOGNAME } || $ENV{ USER } ||
        $ENV{ USERNAME } || 'unknown';

####### Wirebird

# my $test = Wirebird::Resource->new(wirebird => $wirebird, url => 'https://mastodon.social/users/gamehawk.atom');
# $test->retrieve;
# $test = Wirebird::Resource->new(wirebird => $wirebird, url => 'https://wirebird.com/atom.xml');
# $test->retrieve;
# $test = Wirebird::Resource->new(wirebird => $wirebird, url => 'https://mastodon.social/users/gamehawk.rss');
# $test->retrieve;
# $wirebird->merge($test->model->get_statements(iri($test->url), undef, undef));
# print "Is graph: ", ($test->model->get_statements(iri($test->url), undef, undef)->is_graph), "\n";
my $sublist = Wirebird::Resource->new(wirebird => $wirebird, url => $wirebird->server . 'users/' . $username . '/subscriptionlist');

# my $author = Wirebird::Resource->new(wirebird => $wirebird, url => 'https://mastodon.social/users/gamehawk')->retrieve;
# ok(Wirebird::Handler::Profile->shadow($author, 50), 'built shadow profile for author');
# print $author->asTurtle;

my @subscriptions;
ok(push (@subscriptions, $sublist->create({url=>'https://mastodon.social/users/gamehawk.atom'})),'mastodon atom feed created');
is($subscriptions[-1]->url,'https://mastodon.social/@gamehawk', 'resource returns page url from feed create (mastodon atom)');

ok(push (@subscriptions, $sublist->create({url=>'https://mastodon.social/users/gamehawk'})),'mastodon feed created from html');
is($subscriptions[-1]->url,'https://mastodon.social/@gamehawk', 'resource returns page url from feed create (mastodon html)');

ok(push (@subscriptions, $sublist->create({url=>'https://mastodon.social/users/gamehawk.rss'})), 'mastodon rss feed created');
is($subscriptions[-1]->url,'https://mastodon.social/@gamehawk', 'resource returns page url from feed create (mastodon rss)');

ok(push (@subscriptions, $sublist->create({url=>'http://wirebird.com/atom.xml'})), 'plerd atom feed created');
is($subscriptions[-1]->url,'http://wirebird.com/', 'resource returns page url from feed create (plerd atom)');

ok(push (@subscriptions, $sublist->create({url=>'https://cholyknight.com/'})), 'wordpress feed created from html');
is($subscriptions[-1]->url,'https://cholyknight.com/', 'resource returns page url from feed create (wordpress html)');

ok(push (@subscriptions, $sublist->create({url=>'https://cholyknight.com/feed/'})), 'wordpress feed created from xml');
is($subscriptions[-1]->url,'https://cholyknight.com/', 'resource returns page url from feed create (wordpress xml)');


done_testing();
