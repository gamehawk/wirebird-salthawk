#!/usr/bin/perl

use strict;
use Test::More;
    eval "use Test::Pod::Coverage";
binmode(STDOUT, ":utf8");

use local::lib;
use lib( $ENV{'WIREBIRDWD'} || $ENV{'PWD'} )
  . "/lib";    # use local-directory libraries first
use Wirebird;
use Wirebird::Resource;
use Wirebird::Handler::Profile;
use RDF::Trine::Store::Memory;
use RDF::TrineX::Functions -all;

BAIL_OUT('Could not initialize Wirebird') unless my $wirebird = Wirebird->new;
my $username = getlogin || getpwuid( $< ) || $ENV{ LOGNAME } || $ENV{ USER } ||
	$ENV{ USERNAME } || 'unknown';

####### poll

my @subscriptions;
ok(push (@subscriptions, Wirebird::Resource->new(wirebird => $wirebird, url => 'https://mastodon.social/users/gamehawk.atom')),'mastodon feed exists');
ok(push (@subscriptions, Wirebird::Resource->new(wirebird => $wirebird, url => 'http://wirebird.com/atom.xml')), 'plerd atom feed exists');
ok(push (@subscriptions, Wirebird::Resource->new(wirebird => $wirebird, url => 'http://cholyknight.com/')), 'plerd atom feed exists');

use Wirebird::Remote::XML;
foreach my $subscription (@subscriptions) {
	ok(Wirebird::Remote::XML->pollSubscription($subscription, Wirebird::Remote::XML->getSubscribers($subscription)), 'polled ' . $subscription->url);
}

done_testing();
