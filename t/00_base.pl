#!/usr/bin/perl

use strict;
use utf8; # pretty sure this is redundant in modern perl
use Encode;
use Test::More;
    eval "use Test::Pod::Coverage";

use local::lib;
use lib( $ENV{'WIREBIRDWD'} || $ENV{'PWD'} )
  . "/lib";    # use local-directory libraries first
use Wirebird;
use Wirebird::Resource;
use Wirebird::Handler::Profile;
use RDF::Trine::Store::Memory;
use RDF::TrineX::Functions -all;
binmode(STDOUT, ":utf8");

diag('Dev tests for wirebird.pm');
my $username = getlogin || getpwuid( $< ) || $ENV{ LOGNAME } || $ENV{ USER } ||
        $ENV{ USERNAME } || 'unknown';
# dropdb wirebird;createdb -E UTF8 wirebird
BAIL_OUT('Could not initialize Wirebird') unless my $wirebird = Wirebird->new;

####### Wirebird
#ok($wirebird->model->add_statement(statement(iri('http://example.com'), curie('schema:name'), literal(encode('UTF-8', '🥨')))), 'adding UTF8 test statement');
#~ ok($wirebird->model->add_statement(statement(iri('http://example.com'), curie('schema:name'), literal("\N{U+1F968}"))), 'adding UTF8 test statement');
#~ my ($pretzel) = map {decode('UTF-8', $_->[2]->value)} $wirebird->model->get_statements(iri('http://example.com'), curie('schema:name'), undef)->get_all;
#~ is($pretzel, '🥨', 'retrieved UTF8 test');

#ok($wirebird->store->nuke, "nuked store");
#delete $wirebird->{store}; # gotta reset it post-nuke, not something we should build into WB
is($wirebird->port(60), 60, 'Set port() manually.');
ok($wirebird->server() =~ /\:60\//, 'server() uses manually set port()');
delete $wirebird->{'server'};
delete $wirebird->{'port'};

is($wirebird->port(443), 443, 'Set port() to https manually.');
ok($wirebird->server() !~ /\:443\//, 'server() does not put standard port in url');
ok($wirebird->server() =~ /^https/, 'server() recognizes standard HTTPS port');
delete $wirebird->{'server'};
delete $wirebird->{'port'};

is($wirebird->port(80), 80, 'Set port() to SSL manually.');
ok($wirebird->server() !~ /\:80\//, 'server() does not put standard port in url');
ok($wirebird->server() =~ /^http\:/, 'server() recognizes standard HTTP port');
delete $wirebird->{'server'};
delete $wirebird->{'port'};

#ok($wirebird->store('wbstore', RDF::Trine::Store::Memory->temporary_store), 'temporary store() created');

####### Wirebird::Resource

my $page;
ok($page = Wirebird::Resource->new(wirebird => $wirebird, url => $wirebird->server . 'index'), 'initialized root');
BAIL_OUT('Could not initialize root page') unless $page;
is($page->url, $wirebird->server . 'index', 'wirebird index is ' . $wirebird->server . 'index');
ok($page->fromHash({
		'rdf:type' => ['http://rdfs.org/sioc/ns#Site', 'http://schema.org/WebSite', 'http://www.w3.org/ns/ldp#BasicContainer', 'http://www.w3.org/ns/ldp#Resource'],
		'sioc:has_usergroup' => $wirebird->server . 'users/index',
		'sioc:name' => 'Wirebird Test'
	}), 'set root from hash');
ok($page->hasType('sioc:Site'), 'resource is a Site');
ok($page->hasType('schema:WebSite'), 'resource is a WebSite');
ok(!$page->hasType('schema:WebPage'), 'resource is not a WebPage');
ok($page->hasType('schema:Thing'), 'resource is a Thing (inherited)');
ok($page->hasType('sioc:Space'), 'resource is a Space (inherited)');
my $usergroup;
ok($usergroup = $page->asHash->{'sioc:has_usergroup'}, 'resource returns usergroup url via asHash');

ok($usergroup = Wirebird::Resource->new(wirebird => $wirebird, url => $usergroup), 'initalized usergroup');
BAIL_OUT('Could not initialize usergroup page') unless $usergroup;
my $stamp = DateTime->now()->iso8601() . 'Z';

ok($usergroup->fromHash({
	'rdf:type' => ['http://rdfs.org/sioc/ns#UserGroup', 'http://www.w3.org/ns/ldp#BasicContainer', 'http://www.w3.org/ns/ldp#Resource'],
	'sioc:name' => 'Members',
	'dct:created' => $stamp,
	'dct:modified' => $stamp,
	'sioc:has_member' => $wirebird->server . 'users/' . $username,
	'sioc:usergroup_of' => $wirebird->server . 'index'
	}), 'set usergroup from hash');
my ($user) = map {$_->[2]->value} $wirebird->model->get_statements(iri($usergroup->url), curie('sioc:has_member'), undef)->get_all;

is($user, $wirebird->server . 'users/' . $username, 'resource returns user url via get_statements');
ok($user = Wirebird::Resource->new(wirebird => $wirebird, url => $user), 'initialized user');
ok($user->fromHash({
	'rdf:type' => ['http://rdfs.org/sioc/ns#UserAccount', 'http://www.w3.org/ns/activitystreams#Person'],
	'sioc:name' => $username,
	'dct:created' => $stamp,
	'dct:modified' => $stamp,
	'sioc:member_of' => $wirebird->server . 'users/index',
	'sioc:subscriber_of' => $wirebird->server . 'users/' . $username . '/subscriptionlist',
#	'sioc:email' => 'mailto:' . $username . '@' . hostname,
	'sioc:owner_of' => [
		$wirebird->server . 'users/' . $username . '/inbox',
		$wirebird->server . 'users/' . $username . '/outbox',
		$wirebird->server . 'users/' . $username . '/subscriptionlist'
	],
	'as:inbox' =>  $wirebird->server . 'users/' . $username . '/inbox',
	'as:outbox'=> $wirebird->server . 'users/' . $username . '/outbox',
	}), 'set user from hash');

#~ my $user = $usergroup->create(
	#~ {
		#~ 'rdf:type' => ['http://rdfs.org/sioc/ns#UserAccount', 'http://www.w3.org/ns/activitystreams#Person'],
		#~ 'sioc:name' => $username,
		#~ 'sioc:member_of' => $wirebird->server . 'users/index',
		#~ 'sioc:subscriber_of' => $wirebird->server . 'users/' . $username . '/subscriptionlist',
		#~ 'sioc:owner_of' => [
			#~ $wirebird->server . 'users/' . $username . '/inbox',
			#~ $wirebird->server . 'users/' . $username . '/outbox',
			#~ $wirebird->server . 'users/' . $username . '/subscriptionlist'
		#~ ],
		#~ 'as:inbox' =>  $wirebird->server . 'users/' . $username . '/inbox',
		#~ 'as:outbox'=> $wirebird->server . 'users/' . $username . '/outbox',
	#~ }
#~ );

my ($inbox) = map {$_->[2]->value} $wirebird->model->get_statements(iri($user->url), curie('as:inbox'), undef)->get_all;
is($inbox, $wirebird->server . 'users/' . $username . '/inbox', 'resource returns inbox url via get_statements');
ok($inbox = Wirebird::Resource->new(wirebird => $wirebird, url => $inbox), 'initialized inbox');
ok($inbox->fromHash({
	'rdf:type' => ['http://rdfs.org/sioc/ns#Container', 'http://www.w3.org/ns/activitystreams#OrderedCollection'],
	'sioc:name' => 'Inbox for ' . $username,
	'dct:created' => $stamp,
	'dct:modified' => $stamp,
	'sioc:has_owner' => $wirebird->server . 'users/' . $username,
	'as:totalItems' => 0,
}), 'set inbox from hash');

my ($outbox) = map {$_->[2]->value} $wirebird->model->get_statements(iri($user->url), curie('as:outbox'), undef)->get_all;
is($outbox, $wirebird->server . 'users/' . $username . '/outbox', 'resource returns outbox url via get_statements');
ok($outbox = Wirebird::Resource->new(wirebird => $wirebird, url => $outbox), 'initialized outbox');
ok($outbox->fromHash({
	'rdf:type' => ['http://rdfs.org/sioc/ns#Container', 'http://www.w3.org/ns/activitystreams#OrderedCollection'],
	'dct:created' => $stamp,
	'dct:modified' => $stamp,
	'sioc:name' => 'Outbox for ' . $username,
	'sioc:has_owner' => $wirebird->server . 'users/' . $username,
	'as:totalItems' => 0,
}), 'set outbox from hash');

my ($sublist) = map {$_->[2]->value} $wirebird->model->get_statements(iri($user->url), curie('sioc:subscriber_of'), undef)->get_all;
is($sublist, $wirebird->server . 'users/' . $username . '/subscriptionlist', 'resource returns sublist url via get_statements');
ok($sublist = Wirebird::Resource->new(wirebird => $wirebird, url => $sublist), 'initialized sublist');
ok($sublist->fromHash({
	'rdf:type' => ['http://rdfs.org/sioc/ns#SubscriptionList', 'http://www.w3.org/ns/activitystreams#OrderedCollection'],
	'sioc:name' => 'Subscriptions for ' . $username,
	'dct:created' => $stamp,
	'dct:modified' => $stamp,
	'sioc:has_owner' => $wirebird->server . 'users/' . $username,
	'sioc:has_subscriber' => $wirebird->server . 'users/' . $username,
	'as:totalItems' => 0,
}), 'set inbox from hash');

ok($page->asHtml->output, 'root page returns something as html'); # TODO check that it's non-default?
ok($usergroup->asHtml->output, 'usergroup page returns something as html'); # TODO check that it's non-default?
ok($user->asHtml->output, 'user page returns something as html'); # TODO check that it's non-default?
ok($sublist->asHtml->output, 'sublist page returns something as html'); # TODO check that it's non-default?
ok($inbox->asHtml->output, 'inbox page returns something as html'); # TODO check that it's non-default?
ok($outbox->asHtml->output, 'outbox page returns something as html'); # TODO check that it's non-default?

#~ print RDF::Trine::Serializer
    #~ -> new('Turtle')
    #~ -> serialize_model_to_string($wirebird->model);

done_testing();
