title: Rubberducking: more playing with lists
time: 2018-07-02T20:20:31-04:00
published_filename: 2018-07-02-rubberducking-more-playing-with-lists.html
guid: E51531BA-7E56-11E8-A7AC-E6616349DA6E

There is a bunch of different articles out there on handling ordered lists in a triple store.

  * [RDF Collections](https://www.w3.org/TR/rdf11-mt/#rdf-collections) (the standard)
  * [RDF Lists and SPARQL](http://www.snee.com/bobdc.blog/2014/04/rdf-lists-and-sparql.html) (from the author of _Learning SPARQL_)
  * [https://stackoverflow.com/questions/25351911/working-with-sparql-lists-in-dotnetrdf-intersection-of-lists#25369706

An ActivityStreams inbox is a
[streams:OrderedCollection](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-orderedcollection),
though after mostly getting list insertion working I ended up deciding entries
should always be added to the end. (The client can decide to re-order them, but
should always know which ones are newly added that way.) I preserved the other
version as `Inbox.pm.snippet` for this commit only.

I've poked around looking for list handling in the RDF::Trine libraries, with
little success.
[RDF::Trine::Parser::Turtle](https://metacpan.org/source/GWILLIAMS/RDF-Trine-1.019/lib/RDF/Trine/Parser/Turtle.pm),
for instance, has some handling because Turtle syntax can pass lists. But
because RTPT just returns the triples for the parsed Turtle, rather than adding
items to an existing list in an existing store, it just does the most basic
iteration over the list, adding the first/rest nodes. Which _is_ one way of
handling it: just delete all the nodes and re-create them every time an item is
added or removed.

Conversely,
[RDF::Trine::Serializer::Turtle](https://metacpan.org/source/GWILLIAMS/RDF-Trine-1.019/lib/RDF/Trine/Serializer/Turtle.pm)
turns triples into lists, but again doesn't have to do any modification of the
lists. [RDF::Trine::Model](https://metacpan.org/pod/RDF::Trine::Model) has
`add_list`, `get_list`, and `remove_list`; it also (wait for it...) treats lists
as all-or-nothing items.

All of which means I should either start treating lists that way myself, or else
think about writing an RDF::Trine/X library for lists. Only...
[GWILLIAMS](https://metacpan.org/author/GWILLIAMS) is moving from RDF::Trine
over to Attean::, which eventually (when it's stable and documented) I also will
eventually do.

In other news: ironically it isn't the Pi's SD card throwing errors, it's the
spinner in my desktop machine. I still picked up a cheap 32GB thumb drive at
Costco, and shifted the Wirebird directories onto it... including switching to
SQLite in the data directory. Now rather than sshfs-mounting the shared
directory, I'm physically carrying the drive back and forth between the Pi and
desktop, which is less than ideal, but I do get to see the performance
difference between the Pi and the desktop. It's interesting, but I'll probably
move the directory to the desktop machine (sshfs-mounting it the other
direction) once the spinner is replaced.

