title: Cleaning house: updated logging
time: 2018-06-22T23:51:24-04:00
published_filename: 2018-06-22-cleaning-house-updated-logging.html
guid: B2EFDE1C-7698-11E8-BA2F-BE00928660C2

So this happened:

<iframe src="https://mastodon.social/@gamehawk/100250184498971936/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mastodon.social/embed.js" async="async"></script>

I took that as a sign to quit doing my logging with "print" statements.
Most of those are now either converted to Log4perl or deleted. I admit
to some laziness because Wirebird doesn't do any *other* printing, so
it's always been something I can easily grep for. But, okay, it *is*
kind of nice to just flip the flag and not have it chattering at me.

Perlcritic is complaining, again rightly, that Wirebird::Remote::XML's
_nodehash is a big nest of elsifs. Other than that, and some missing
documentation, things are pretty clean to close out the week.

The commit is rather lengthy because I have been reducing the number of
external calls. Turns out FontAwesome, which isn't even used yet, is a
considerable number of files. It also probably shouldn't be embedded in
the repo, so eventually I'll get around to gitignoring it.

