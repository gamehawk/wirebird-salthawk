title: Coding out loud: SPARQLy inbox
time: 2018-06-21T22:52:09-04:00
published_filename: 2018-06-21-coding-out-loud-sparqly-inbox.html
guid: 41CA5D56-75C7-11E8-A843-01A28067A38E

I've messed around a little more with SPARQL, and added plugins to the
`Resource::asHtml` function. `Wirebird::Handler::Inbox` now does a
custom query. It's incomplete, but it's progress.

Sadly, I don't think I can roll everything into a single query; the
inbox structure has too many nested loops (inbox metadata, then inbox
items, then authors of the items, attachments to the items, etc.) If I
could flatten all that into a single query, then I would give some
thought to backing it out of the plugin system. `Wirebird::Resource`
could just look up the query and the matching template for a resource's
type.

Of course, this continues to assume I need to write a full HTML back
end, rather than just serving up a SPARQL endpoint and letting the
client do the heavy lifting. Maybe I'm old-fashioned, but I think I
still do.

