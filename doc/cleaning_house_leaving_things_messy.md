title: Cleaning house: Leaving things messy
time: 2018-07-06T19:57:52-04:00
published_filename: 2018-07-06-cleaning-house-leaving-things-messy.html
guid: 65416F9C-8178-11E8-B346-27A88D531F8F

It has been a week for working on Wirebird in five- and ten-minute increments,
with insufficient sleep, and it shows.

I've never been happy with the Dancer2 server passing things to
`Wirebird::Resource->create()` as a `RDF::Trine::Model` in part because I have
to predeclare the subject URL of the created triples. But I didn't want to pass
the Dancer object because I didn't want `Wirebird::Resource` to be
Dancer-dependent (since it's called from non-web processes already). But then I
decided, good grief, I'm already overloading the `$parms` variable in a
way-too-hacky manner. Sometimes it's a `Resource` object, sometimes it's a naked
URL, sometimes it's a `Model`... yeah, that's really getting bad.

So now it's going to accept proper named parameters, but the changeover is
entirely incomplete.

In other cleanup news, I've fully moved Plerd to the Cruzer, and the Pi is
powered down. Soon, possibly this weekend, I'll get around to fully installing
it in its little case so that it's entirely portable. Then I can just pop the
Cruzer into it as a somewhat eccentric laptop.

