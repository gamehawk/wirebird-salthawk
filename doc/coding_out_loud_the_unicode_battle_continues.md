title: Coding out loud: the Unicode battle continues
time: 2018-06-07T12:42:19Z
published_filename: 2018-06-08-coding-out-loud-the-unicode-battle-continues.html
guid: 4956676E-6B26-11E8-AD0E-B6270356252B

Perl makes some compromises in its Unicode implementation to avoid 
breaking old code and boy it sure makes new code work funny sometimes.

Not too much to say today; coding right now is just a matter of:

  * making sure everything in the Wirebird::Remote::* hierarchy processes retrieved things to consistently be UTF-8
  * making sure RDF::Trine::Store consistently returns UTF-8 encoded/escaped the same way regardless of underlying DBI
  * making sure templates do appropriate escaping on display

It's tedious, and there are all sorts of entertaining pitfalls (like 
realizing that UXTerm on my desktop machine displays things differently 
than the Pi does, and that I need to put everything in tests and not 
just inspect it in console).

