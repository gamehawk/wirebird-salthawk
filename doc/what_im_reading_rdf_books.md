title: What I'm reading: RDF books
time: 2018-05-21T11:12:29-04:00
published_filename: 2018-05-21-what-im-reading-rdf-books.html
guid: 6106C29E-5D09-11E8-BC46-BAB41BAEACAC

Still not a lot of code change today, again in part because of lack of 
time and because I've been doing more reading than writing. Here's 
what's been occupying me, and why.

### Learning SPARQL

On the one hand, SPARQL ("sparkle," don't @ me) is the real power that 
storing everything in RDF trines gives. On the other hand, there's some 
tension in that I want to keep Wirebird lean enough to run in the 
background of a desktop machine, or on a Raspberry Pi, or even directly 
on a phone. Still, I want to be able to call on it for special things 
like search requests and such, so I've been reading [Learning SPARQL](http://learningsparql.com/).

### Programming the Semantic Web

I've also been reading [Programming the Semantic Web](https://amzn.to/2LdH6md)
which is a little depressing since it keeps referring to services that 
don't exist anymore (it's a 2009 edition). And all its examples are in 
Python, which... I really don't want to change horses midstream, but 
Python just keeps coming up as a good thing to know.

###  Semantic Web for the Working Ontologist: Effective Modeling in RDFS and OWL

Up next is [Semantic Web for the Working Ontologist](https://amzn.to/2Lg9zaS)
which I haven't actually looked into yet, but the subtitle sounds
promising.

_(Those Amazon links are affiliate ones. Fun fact: I've had that 
affiliate link for ten years or so and have yet to reach the minimum 
for a payout.)_

