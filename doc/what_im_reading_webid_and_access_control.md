title: What I'm reading: WebID and Access Control
time: 2018-06-05T21:40:04-04:00
published_filename: 2018-06-05-what-im-reading-webid-and-access-control.html
guid: 88E91ECE-692A-11E8-A8B8-AFC30256252B

I regularly add useful links to the end of the sidebar list and, in 
keeping with the things needed for MVP I've been looking at 
authen/authz. It's a contentious issue in RESTful design, and layering 
RDF on top makes things even more fun. So long as there's a 
machine-readable way for a smart client to figure out how to log in, 
I'm not going to insist on pure statelessness.

WebID (a/k/a FOAF+SSL or FOAF+TLS) doesn't seem to have gained a great 
deal of traction, but it's still being actively developed at MIT and 
it's well represented in CPAN libraries for RDF so maybe it's just been 
under my radar. At any rate, it's, well, still being actively developed 
at MIT and it's well represented in CPAN libraries for RDF, so I'm 
leaning toward using it.

One thing it doesn't do, and neither does ActivityPub or the Mastodon
API or anything else I can think of: allow creation of a local account.
I can see why one might not want automated account generation, but I
see Wirebird as an entry point - i.e., a place to *originate* one's
identity. That might be a little overambitious, but I'll see where the
reading takes me.

