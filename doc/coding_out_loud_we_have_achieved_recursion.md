title: Coding out loud: we have achieved recursion
time: 2018-05-24T11:21:34-04:00
published_filename: 2018-05-24-coding-out-loud-we-have-achieved-recursion.html
guid: 25267724-5F66-11E8-9CFF-400EAA767D5B

Not literally, just visually:

![Screenshot of the Inbox](2018-05-24_Inbox.png)

The [database 
preloader](http://wirebird.com/2018-05-16-preloading-the-database.html) 
now builds the site, subscribes to one feed (this blog, using the same 
call as is performed when POSTing to the SubscriptionList), and polls 
that feed once.

It's still pretty hacky, but it works. Theoretically I could write a 
little cron job to continue to poll the feed(s), and the inbox would be 
continuously updated. I could subscribe to more feeds, and so forth.

I'm getting, as the current fashionable phrase says, way out over my 
skis when it comes to tests, documentation, and so forth, so tomorrow 
will be all housecleaning. Next week, all kinds of bug fixes. Here are 
some of the big ones.

  * Taking a deeper look at the plugins. Currently objects aren't (re)blessed into those libraries, which is not how I usually do it and seems like a misuse of the OO system.
  * Figuring out best-practices for representing ordered lists in RDF (Current hack is to sort the inbox entries by url, which works for a single poll of a single already-sorted feed but breaks down completely after that).
  * Cleaning up the feed entry content instead of dumping it raw into the RDF object (which breaks encoding, url bases, etc.)
  * Making the plugins select the templates. Currently it just concatenates all the types it finds, but container_orderedcollection is not *always* going to be an Inbox down the road.

