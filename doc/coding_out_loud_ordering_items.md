title: Coding out loud: ordering items
time: 2018-06-25T22:46:25-04:00
published_filename: 2018-06-25-coding-out-loud-ordering-items.html
guid: 1E1B9ED4-78EB-11E8-8BC9-FA62928660C2

A [month ago](https://mastodon.social/@gamehawk/100260380408157791) I alluded to _"Figuring out best-practices for representing ordered lists in RDF"_ which I need to revisit. Triples have no order; if you want resources to be ordered you have to give them a sequence via other triples. There's `rdf:first` and there's [a whole paper on the subject](http://infolab.stanford.edu/~stefan/daml/order.html) and there's `rel:next` and there's `schema:position` and probably any number of other thoughts and definitions, so it's just a matter of settling on one.

[rdf:first](https://www.w3.org/TR/rdf-schema/#ch_first) requires a little bit of care to make sure that any `rdf:rest` links stay synchronized and non-circular. It has the added advantage of being chainable in SPARQL queries. `Schema:position` and other explicit position-number fields have some advantages (I'm partial to .newsrc-style run list management) but that doesn't allow inserting items anywhere but the end of the list. There's some internal advantage to that (keeping the sequence in which items were added to the inbox, and providing a unique identifier for each inbox item) even if it's not the eventual display order. I'm using both methods, and absent any better standard I'll start with the `schema:position` name.

Inserting items mid-list gets fun, but luckily for me Bob duCharme (of _Learning SPARQL_ in the sidebar there) has written [a blog post about RDF lists](http://www.snee.com/bobdc.blog/2014/04/rdf-lists-and-sparql.html) where he walks through it. And ends with

  I won't remember the syntax of these queries without reviewing them as written here, but I know that I can copy them from here and paste them elsewhere with minor modifications to perform these basic list manipulation goals.

which, I have to admit, is about 98% of the reason _I_ blog about technical stuff.

