title: Coding out loud: Digging into Solid and Hydra
time: 2018-07-05T21:38:25-04:00
published_filename: 2018-07-05-coding-out-loud-digging-into-solid-and-hydra.html
guid: 46A5240E-80BD-11E8-8B38-AE738D531F8F

Today (such as it was) is just a continuation of [yesterday's work](2018-07-04-coding-out-loud-solidifying-the-feed-reader.html).

I have turned off static-file handling for now, ironically _after_ changing it
to write RDFJSON, RDFXML, and Turtle. Linked Data Platform, and therefore Solid,
require a lot of headers and by the time those are calculated, serving the body
itself dynamically is fairly trivial. That doesn't give anything concrete for
ETag to build off of though, so that will probably change back. In the meantime,
it's starting to serve some of the other LDP-compliant headers.

I also got started on the Friday cleanup early, and have begun shifting todo's
into Gitlab issues. Not sure I'll get too committed to that, though, because I
think I'd like to self-host Gitea instead.

