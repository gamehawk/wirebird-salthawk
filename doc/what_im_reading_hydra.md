title: What I'm reading: Hydra
time: 2018-06-27T22:16:01-04:00
published_filename: 2018-06-27-what-im-reading-hydra.html
guid: 33B5A0F2-7A79-11E8-8CDE-D94C928660C2

_(must not make obvious Marvel joke must not make obvious Marvel joke)_

I had looked at Hydra awhile back, but the four-year-old "latest" blog post on
their [W3C page](https://www.w3.org/community/hydra/) and [list of
talks](http://www.hydra-cg.com/#community) that end around then too had me
assuming it was YA inactive project. Ran across a link to it, dug a little
deeper, and turns out it's still being developed.

Hydra is adding some vocabulary to improve on [inferring an API from
machine-readable standards](2018-05-13-a-first-post-on-wirebird.html). Some of
this duplicates what's already available, such as allowing a client to figure
out what http actions are available for a given URL - but it allows discovery
from a triple-store query instead of having to make an http request and
examining the headers. Other parts explicitly lay out, for instance, the type of
resource a particular url expects for a request, and what it will return. Hydra
definitely earns a place in the sidebar list of this blog.

