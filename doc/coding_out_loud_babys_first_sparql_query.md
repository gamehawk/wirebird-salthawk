title: Coding out loud: baby's first SPARQL query
time: 2018-06-20T21:10:37-04:00
published_filename: 2018-06-20-coding-out-loud-babys-first-sparql-query.html
guid: E7DEBBC0-74EF-11E8-9ED2-AD3C8067A38E

I'm back to _Learning SPARQL_, after setting it (figuratively) aside
when Safari lost my bookmark. Now I'm back to it in paper. After
spending yesterday failing to set up a SPARQL endpoint with
[RDF::Endpoint](https://metacpan.org/pod/RDF::Endpoint), I just decided
to mess around with queries embedded in Perl (in the
`script/Playground.pl` workspace).

	PREFIX sioc: <http://rdfs.org/sioc/ns#> 
	PREFIX stream: <http://www.w3.org/ns/activitystreams#>
	PREFIX atom: <http://www.w3.org/2005/Atom/>

	SELECT *
	WHERE {
	 ?inbox sioc:has_owner <http://habrok:6809/users/silver> .
	 ?inbox stream:item ?item . 
	 ?item stream:object ?remote .
	 ?remote atom:id ?url .
	 ?remote atom:title ?title .
	 ?remote atom:published ?published .
	 ?remote atom:content ?content .
	}

It's a pretty boring query, but it will pull up all of the information
in my Inbox. Or anything else that `has_owner` me, so it's far from a
finished product. Next step will be setting rules so that rss:titles
are the same as atom:titles and so forth, and so that the author info
from either the entry or the parent feed gets pulled in.

