title: Steps toward a minimum viable product
time: 2018-05-13T23:36:00-04:00
published_filename: 2018-05-13-steps-toward-a-minimum-viable-product.html
guid: EBF1C206-5727-11E8-9792-E35B647B1411

### First, a feed reader

A basic feed reader is pretty straightforward: fetch RSS and Atom
feeds and display them. That's it.

Marking items read is optional: Facebook timelines just display them in
(kind of!) reverse-chron, and you just stop reading when you run into
something you've already seen. _(Facebook sabotages this by shuffling
things around when, I guess, they have new *comments* on them, but
that's a different issue.)_

Being able to add and delete feeds is optional, if it allows reading an
OPML file.

Parsing feeds can be an exercise in edge cases, but luckily there's
already XML::Feeds so that work has been taken care of.

### Second, a blog publisher

Publishing a (static) blog is an easy next step: the feed reader is
already formatting posts. The publishing side just has to read entries
from something other than an XML file. I'm going to start with
Plerd-style markdown files, because that will allow me to start writing
this blog with the intention of being able to import it.

### Third, blog comments

I haven't decided how to implement this yet. Webmentions are a
possibility, but so would be to start work on the default browser
client. Of course, the most basic method would be to have a simple HTML
form that does a POST, but without some kind of authen/authz system
that would be pretty open to abuse. We'll see.

