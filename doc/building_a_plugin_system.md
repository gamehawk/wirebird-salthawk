title: Building a plugin system
time: 2018-05-18T11:53:16-04:00
published_filename: 2018-05-18-building-a-plugin-system.html
guid: 93FC3FBA-5AB3-11E8-B60E-F51F2D578964

Another short "day," but there is now a `Wirebird::Resource` object 
instead of resources being passed around in `Wirebird.pm` as various 
bits and pieces at different levels of processing. I haven't pruned 
that stuff out of the original library yet, and some of the POD in the 
new library doesn't reflect the change, but it's getting there.

When a Resource object is created, it can either have an url or a 
hashref passed to it. Eventually the hashref will need to be an object 
holding JSON-LD, RDF, or some other recognized format but right now 
it's the pseudo-JSON hash that I started out using.

The Resource uses plugins in the `Wirebird::Handler::` hierarchy, of 
which there is exactly one (partial) so far. 
`Wirebird::Handler::SubscriptionList` has a priority() sub which will 
check to see if the resource has a sioc:SubscriptionList type at which 
point it returns a 100. Not exactly subtle. bidCreate() will, 
similarly, return 100 as its bid on handling something posted to a 
SubscriptionList, and create() is the handler.

When, eventually, `Wirebird::Handler::Container` exists, it will return 
a lower priority() and bidCreate() when it sees a SubscriptionList. 
`Wirebird::Resource` will process down the handlers in descending bid 
order, and each bidder's create() can look at the resource's current 
status() (which starts as 501 Not Implemented) to decide whether to do 
further processing. For example, it's fairly likely that Container will 
see that the SubscriptionList handler has changed the status to a 3xx 
pointing to the new resource and not do anything further... or maybe 
SubscriptionList doesn't set the has_container/container_of because it 
knows Container will do it down the line.

Right now `Wirebird::Resource` doesn't do anything with something put 
out to bid that still returns 501, but as the TODO says, eventually it 
will be able to say "okay, according to the standard this X is a valid 
resource, it points to the Y in its `foo_of` attribute, and Y has a 
valid inverse attribute of `has_foo` that X is valid as so... save it. 
(This assumes the user is authorized to POST anything to Y, of course.)

In fact, `Wirebird::Handler::SubscriptionList` could delegate most 
stuff right back to `Wirebird::Resource`. Presently, the thing it's 
doing right now that's special is handling a bare, remote url. (I mean, 
it isn't _yet_, but that's what it's there for.)

