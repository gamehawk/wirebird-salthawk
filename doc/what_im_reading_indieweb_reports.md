title: What I'm reading: IndieWeb Summit reports
time: 2018-06-28T20:20:29-04:00
published_filename: 2018-06-28-what-im-reading-indieweb-summit-reports.html
guid: 3AC7A43A-7B32-11E8-9A0F-476D928660C2

I feel like I should have gone to IndieWeb Summit, but at this point my Patreon
isn't at a level that it can pay for things like that. (Admittedly a
contributing factor to that is that I don't *have* a Patreon.) But a couple
people I know [of] did and so I've been vicariously experiencing it, via livetooting *(it's a Mastodon thing)* and remote video.

  * [I Went to The IndieWeb Summit 2018! &mdash; jackyalciné](https://jacky.wtf/weblog/indieweb-summit-2018/) - "I attended my first IndieWeb Summit in Portland this year."
  * [I attended IndieWeb Summit 2018](http://fogknife.com/2018-06-28-i-attended-indieweb-summit-2018.html) - "Notes and thoughts from the eighth annual IndieWeb conference, held in Portland, Oregon."

No significant commit today; most of my IndieWeb reading today took place while
sitting on a college campus waiting for my son to finish an advising
appointment. I'm also back to reading _Learning SPARQL_ so I know what I'm doing
with these sequences. I really need to just stop poking at code and finish
reading that - no more of this reading a page or two and going "ooh, I need to
try this out!"

