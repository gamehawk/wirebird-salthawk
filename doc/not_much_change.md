title: Not much change
time: 2018-06-18T21:52:12-04:00
published_filename: 2018-06-18-not-much-change.html
guid: 62AAB5FE-7363-11E8-84E1-BC01E340A448

The blog source is in the repo now, but otherwise not much progress -
largely due to the need to drive across the state and back today. I've
fine-tuned some bits of the XML feed processing, but that's it.

The next step is to get the `poll_subscriptions` stub filled out again.
It's kind of exciting to see the triples dumped to screen. Once the
polling sub puts them into the permanent store, it's time to start
playing around with some SPARQL queries and see if it's as magical as
its backers claim.


