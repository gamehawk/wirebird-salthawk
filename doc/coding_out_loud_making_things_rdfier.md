title: Coding out loud: Making things RDFier
time: 2018-05-22T13:50:17-04:00
published_filename: 2018-05-22-coding-out-loud-making-things-rdfier.html
guid: 96E0EEF0-5DE8-11E8-995D-CBED3C45EAD3

Still doing a lot of reading, but also doing a little coding.

I've been treating the RDF store(s) a bit like a key/value store rather 
than getting into any more depth, and to an extent that won't change 
just yet. But I've been thinking about how Wirebird::Resource needs to 
handle its data, and I've made some changes.

The authoritative store is an RDF::Trine::Store::DBI (in my case, 
Postgres-backed, but it doesn't really care - in fact, it could be a 
remote store or whatever). For performance reasons, Wirebird::Resource 
pulls in a memory version of that. It's been a hashref (kind of 
JSON-LD) but I'm also playing around with using an RDF::Trine::Model 
based on a memory store. So right now it's got all kinds of things in 
there since the original versions of all the Wirebird::Resource 
subroutines are still in Wirebird.pm waiting to be cleaned up.

The progress toward a [minimal feed 
reader](2018-05-14-building-a-minimal-feed-reader.html) is a little 
slow at the moment. Technically, you can submit an url into the form, 
but it doesn't get very far - it will announce in console that it's 
found an XML::Feed, but doesn't process it just yet. That'll be the 
next step, along with the subroutine cleanup.

