title: Coding out loud: modeling
time: 2018-06-26T22:08:59-04:00
published_filename: 2018-06-26-coding-out-loud-modeling.html
guid: 0DE757BE-79AF-11E8-8653-2A1E928660C2

I spent yesterday taking all the `model()` and `merge()` stuff out of
`Resource.pm` and its plugins, and I spent yesterday putting it back
(kinda).

Keeping things synchronized was just a little too awkward, especially
once things stop being single-threaded. I realized that most of the
processing of a temporary model was going to happen within a single
subroutine and didn't need to persist. One exception is when a resource
is being created, so `Resource.pm` gets a `model()` statement again,
but unless a memory store is specifically created, it just passes
through to the permanent store.

That took most of my time, so while I have a little bit of the
[item-ordering logic](2018-06-25-coding-out-loud-ordering-items.html)
going in, it definitely doesn't work yet.

