title: What I'm reading: more on identity
time: 2018-06-13T21:09:09-04:00
published_filename: 2018-06-13-what-im-reading-more-on-identity.html
guid: 8A955774-6F6F-11E8-A28C-21EC01E0D2FF

I've been digging around in the [Solid repo](https://github.com/solid) 
to see how MIT/TBL have been solving this. The answer, again, is 
"generally handing it off to already-existing identities." There's a 
little bit, though: a [signup app](https://github.com/solid/solid-signup) 
and, perhaps most relevant, a [list of identity providers in JSON format.](https://github.com/solid/solid-idp-list) 
It's not (yet?) any kind of machine-readable standard, and as of this 
writing its only entry, Databox.me, serves an nginx default page on its 
http port and a demo on its https, but it's a start.

[Server capability discovery](https://github.com/solid/solid/blob/master/proposals/server-capability-discovery.md) 
mentions Turtle and JSON-LD being on the roadmap, so that's promising.

