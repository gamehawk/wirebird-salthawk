title: Cleaning house: adding the docs
time: 2018-05-25T12:12:15-04:00
published_filename: 2018-05-25-cleaning-house-adding-the-docs.html
guid: 63C87A6A-6036-11E8-8141-6233AA767D5B

I didn't get as far into cleanup as I had hoped today - I got the basic 
documentation up to date, cleaned up a lot of orphaned subroutines (and 
entire libraries), but the only additional testing I did was to tack 
the Test::Pod::Coverage onto the end of the lonely initial test 
routine.

It's not awful -- Birdfeeder.pl is essentially a test suite of its own, 
lacking only the Test::More wrapper and deliberately-wrong attempts 
(which is a major lack, admittedly). Over the course of next week, I'll 
move Birdfeeder into a test script and remedy that lack, and also move 
all my TODO comments into tests.

The docs are still not-great, but there's at least a little description 
of each subroutine.

