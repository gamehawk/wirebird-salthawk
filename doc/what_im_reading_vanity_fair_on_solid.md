title: What I'm reading: Vanity Fair on Solid
time: 2018-07-03T22:50:26-04:00
published_filename: 2018-07-03-what-im-reading-vanity-fair-on-solid.html
guid: 00EEA928-7F35-11E8-A139-D3686349DA6E

I mentioned Solid in [an earlier post](2018-06-13-what-im-reading-more-on-identity.html)
and have been poking through its various applications off and on. It is
essentially the same principles I laid out for Wirebird in the
[first post](2018-05-13-a-first-post-on-wirebird.html) so I feel kinda
validated: it's not just some kind of 2008 idea that everybody has already
tried and discarded and I'm just late to the party on.

![Tim Berners-Lee, Web Developer](Understatement.jpg)

Vanity Fair(?!) had
[an interview with Sir Tim](https://www.vanityfair.com/news/2018/07/the-man-who-created-the-world-wide-web-has-some-regrets)
yesterday, brought to my attention by Eugen after a one-hour interview with him
was boiled down to, well:

![One hour, one sentence](OneHourOneSentence.png)

No real code changes today; it is my birthday and also the day before we ship
son across the country to visit an uncle and aunt so we've taken a day off. But
now that I more-or-less have feed polling happening, getting it serving Turtle
et al is the next priority - and doing so in a solid-compliant manner.

