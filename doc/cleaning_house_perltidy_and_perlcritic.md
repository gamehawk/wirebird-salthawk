title: Cleaning house: perltidy and perlcritic
time: 2018-06-08T10:14:41-04:00
published_filename: 2018-06-08-cleaning-house-perltidy-and-perlcritic.html
guid: 495F0176-6B26-11E8-AD0E-B6270356252B

My development environment these days is a Raspberry Pi 3B+, which is 
rather depressingly close to the specs of my regular desktop machine. 
It lives out in the media room, hooked up to the TV, on the network via 
wifi, and I have it crosslinked to my desktop machine via sshfs. Just 
by way of keeping my environment consistent, I use Geany to edit it 
regardless of which keyboard I'm at.

The Perl run environment is on the Pi, by way of keeping me honest as 
far as performance goes - inefficient code makes itself felt very 
quickly. Disk access is the most noticeable.

All of which is a lengthy excuse for why I'm just now getting around to 
installing Perl::Critic. Perltidy has been around but not reliably used 
("get it running in Geany" is off on the yak-shaving list).

Perlcritic caught a missing "use strict" in one library, but otherwise 
I passed `-gentle` with flying colors. `-stern` was a whole different 
matter, but it now almost passes `-harsh` which, honestly, is as far as 
I'm likely to go. The one exception is 
`Wirebird::Handler::XMLFeed::pollSubscription()` which accuses of high 
complexity and... it's not wrong. That subroutine is one big TODO on 
several levels.


