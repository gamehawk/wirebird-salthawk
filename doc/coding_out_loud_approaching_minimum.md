title: Coding out loud: approaching minimum
time: 2018-05-30T10:00:05-04:00
published_filename: 2018-05-30-coding-out-loud-approaching-minimum.html
guid: C101FDDA-6411-11E8-8F05-AB84A11C8E6A

As of now, Wirebird will:

  * build a basic website, using the default user
  * allow feeds to be added to the subscription list
  * poll the feeds and add any new entries to the feed and to the subscriber's inbox
  * allow entries to be deleted from the inbox

Things it doesn't do:

  * any authen/authz whatsoever
  * deleting subscriptions
  * respond well to anything that isn't an Atom or RSS feed
  * import (or export) OPML
  * correctly serve anything but the HTML version of anything

The template is also very minimalist, so technically to delete inbox 
entries (i.e. "mark as read) you have to click the form button, which 
will take you to the now-empty item page; back-arrow to the inbox page, 
which will be cached; refresh to actually remove the item from the 
screen. Obviously, some ajax would normally happen there. I'm in no 
hurry to do anything on the front end (as you can tell by the 
[Sketchy](https://bootswatch.com/sketchy/) appearance) but I might 
write a little jquery in to handle that at some point.

There are still plenty of straight-up bugs, too: no attempt is made to 
process the content of the entries yet. The encoding/escaping is 
clearly a little incompatible between LWP::UserAgent and XML::Feed and 
RDF::Trine and HTML::Template and Dancer, so I'll need to test it at 
each step to make sure everything flows properly.

![We have some bad escaping going on here](2018-05-30-bad_escaping.png)

I'm not ready to make the switch from Tiny Tiny RSS yet, but it's a 
pretty good start for a couple of (very) part-time weeks.

