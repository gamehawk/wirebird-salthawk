title: Cleaning house: back to testability
time: 2018-06-15T22:03:17-04:00
published_filename: 2018-06-15-cleaning-house-back-to-testability.html
guid: 6FC00386-7109-11E8-B59D-2FD301E0D2FF

Yesterday's [work on converting XML to
triples](2018-06-14-rubberducking-translating-files-to-structured-data.html) is
mostly finished; most attributes are dumped but that's fairly trivial to fix...
once I decide what it should be doing with them.

`Wirebird::Remote::XML` is still dumping a lot of diagnostics to console, but
I've restored the testing scripts. It's not passing, of course, but it's happily
subscribing to a feed. Feed polling is still part of W::R::XMLFeed, which is
still `XML::FeedPP` based. `Wirebird::Remote::XML` builds a model including the
entries so that'll be a quick conversion.

It's too late for today's commit push, but I think I'm going to move the blog
source into the repo; it is, after all, the only changelog at this point.

