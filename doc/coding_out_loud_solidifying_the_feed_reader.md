title: Coding out loud: Solidifying the feed reader
time: 2018-07-04T20:36:57-04:00
published_filename: 2018-07-04-coding-out-loud-solidifying-the-feed-reader.html
guid: 85A88006-7FEB-11E8-A01E-89856349DA6E

Another day of not so much coding (after a 4am trip to the airport), but I have
started cleaning up things like serving RDFJSON, RDFXML, and Turtle. While I'm
at it, I might as well get around to building the rest of the headers to make it
a proper [Linked Data Platform](https://www.w3.org/TR/ldp-primer/) arrangement,
and therefore more aligned with [Solid](https://solid.mit.edu/).

On which note, I've joined the Gitter chat rooms for Solid, which I hadn't
previously known about until  [the Vanity Fair
article](2018-07-03-what-im-reading-vanity-fair-on-solid.html).  I'm not a fan
of Yet Another Chat Room, but hopefully that will fill in some of the gaps in my
knowledge.

