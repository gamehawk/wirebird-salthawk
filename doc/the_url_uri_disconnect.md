title: The URL/URI disconnect
time: 2018-05-23T10:58:47-04:00
published_filename: 2018-05-23-the-urluri-disconnect.html
guid: CBE24D5E-5E99-11E8-85F9-57103D45EAD3

I ran into a little complication with making new Wirebird::Resource 
objects that don't appear in the main store yet, and it's causing me to 
reconsider some things. First, a little background.

RDF triples can refer to URIs which are not necessarily URLs - that is, 
*identifiers* that do not necessarily *locate* a thing that can be 
served by a web server.

![Ceci n'est pas une pipe.](MagrittePipe.jpg)

For instance, I might have a URI that identifies a pipe. Not a picture 
of a pipe, but an actual physical object. It might be a resolvable http 
URI that looks like an URL until you ask the server for it, at which 
point the server serves a 303 See Other with the URI to an image. When 
the browser requests _that_ URI, it is also a URL so the server can 
serve a JPG *located* there. Or the dance might continue: the server 
could serve a 303 again pointing to an URL for [a web page about the 
image](https://en.wikipedia.org/wiki/The_Treachery_of_Images).

I've kind of hand-waved this: "I'm building a RESTful system, so of 
course all my URIs need to be URLs" etc. etc. But then 
Wirebird::Handler::SubscriptionList accepted a URI that my browser 
POSTed and... now what?

It built a child Wirebird::Resource, but the only URI it had was to the 
[Atom feed](atom.xml) for this blog. I had been building the URL for 
the new resource from the URL to the blog itself - the home page, not 
the feed, since the feed location for a blog can change when the blog 
software does. But I clearly don't have that until I have retrieved the 
feed, and I don't retrieve the feed until I have a Wirebird::Resource 
to do it from, and if I'm now storing the feed data in an RDF store I 
need the subject URI. But in this case the URI is dependent on the 
SubscriptionList's URI, so I can clearly not use the Wirebird::Resource 
that's in front of me.

![Vizzini logic](vizzini-princess-bride.jpg)

So while I was debating that, I just gave the triples the feed URL, and things are fine. So far.

	<http://wirebird.com/atom.xml>  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>       <http://rdfs.org/sioc/ns#WebLog>
	<http://wirebird.com/atom.xml>  <http://rdfs.org/sioc/ns#name>  "Wirebird"
	<http://wirebird.com/atom.xml>  <http://rdfs.org/sioc/ns#feed>  <http://wirebird.com/atom.xml>
	<http://wirebird.com/atom.xml>  <http://rdfs.org/sioc/ns#has_container> <http://habrok:6809/users/silver/subscriptionlist>
	<http://wirebird.com/atom.xml>  <http://rdfs.org/sioc/ns#link>  <http://wirebird.com/>

Eventually I'll want to have a URI (that is also a URL) for the local 
representation of the feed, because it will be much easier to issue a 
DELETE against that representation than to PUT an entire copy of the 
subscription list minus that one feed. (In fact, for a multi-user 
system I'll want multiple URLs representing not the feed but a single 
user's subscription to that feed.) But for the individual feed entries, 
maybe not - they can live in the store under their native URLs, and 
each subscriber's Inbox can have a Create action with the entry as the 
object. So maybe my store has URIs that aren't (local) URLs.

That said, I think the way to go here is to not have Wirebird::Resource 
_or_ the handler plugins handle those non-local resources - I think I'm 
going to break Wirebird::Resource::pollRemote into a whole separate 
object. With its own plugin systems to handle all the things it might 
encounter in a wild URL - XML syndication feeds, RDFa-enabled web 
pages, JSON, HTML never designed for machine readability, media types, 
and so forth.

Truly, I have a dazzling intellect.

