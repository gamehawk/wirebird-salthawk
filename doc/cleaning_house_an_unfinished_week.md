title: Cleaning house: an unfinished week
time: 2018-06-29T23:21:26-04:00
published_filename: 2018-06-29-cleaning-house-an-unfinished-week.html
guid: AC6AFE2E-7C14-11E8-9881-741B928660C2

It was a pretty unsatisfying week all around, at least as far as coding
progress goes. I did learn a lot, at least.

I can't believe there aren't any shortcuts in the RDF::Trine libraries
to handle ordered lists. It's so unbelievable, in fact, that I'm pretty
sure it's there and I've just overlooked it. Every now and then I have
to go back through the CPAN libraries and go, "oh NOW I see what that
does."

At any rate, today I did the weekly perlcritic (passing, but only at
`stern`) and perltidy and docs test, so that's all tidied up. Polling
subscriptions is still broken, but the queries are *this* close to
working.

