title: Coding out loud: the Wirebird base library
time: 2018-05-15T21:35:25-04:00
published_filename: 2018-05-15-coding-out-loud-the-wirebird-base-library.html
guid: 687A7872-58A9-11E8-BD6C-DE78A429803E

Influenced by [Jacky's public development](https://jacky.wtf/weblog/), 
I'm throwing my own working-in-progress out there. Not livestreaming my 
coding, though, nobody wants to see that.

I'll be putting a new repo up on [Gitlab](https://gitlab.com/gamehawk) 
real soon now. So far it's not much, though I have a lot of other code 
waiting to get converted over from JSON files to RDF database store.

What it does right now: `Wirebird.pm` is the library, and is mostly a 
conglomeration of helper functions and syntactic sugar, over a 
Postgres-based [RDF::Trine](https://metacpan.org/pod/RDF::Trine) store. 
(When [Attean](https://metacpan.org/pod/Attean) gets a little more 
mature, I'll probably switch over.)

Although RDF can store some complicated structures, nothing Wirebird 
builds so far is more complicated than what can be described in basic 
JSON-LD. So for speed, most of the HTML and whatnot is built and served 
by slurping up the static JSON file.

The library is doing some basic RESTful verb handling. The framework 
for validating incoming data is in place but not doing anything yet 
(the inline comments address some of this). As I described in my [first 
post on the subject](2018-05-13-a-first-post-on-wirebird.html) the 
nature of REST+structured data means the generic handler can just Do 
The Right Thing without special custom coding, at least within a 
user-controlled space.

For times when a little customization *is* needed, such as when an 
authenticated user is trying to POST something outside the user's 
directory, the library has a plugin system. Each plugin will have a 
subroutine where it examines the JSON file for that url and comes back 
with a priority bid. So maybe you post a `sioc:Weblog` to a 
`sioc:SubscriptionList`. The plugin for generically posting `sioc:Item` 
to `sioc:Container` throws a lowball bid, but the one specifically for 
Weblogs to SubscriptionLists says "YES THAT IS MINE!". Same goes for 
rendering - generically the library looks for a template that matches 
the `@type` of the data, but a plugin could easily say "You know what, 
this is an Inbox and not just any old `streams:OrderedCollection`, use 
a custom template."

Anyway. Not much of this is specifically relevant to the [first 
step](2018-05-14-building-a-minimal-feed-reader.html) but you know, 
gotta build on a solid foundation.

