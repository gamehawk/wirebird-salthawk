title: A first post on Wirebird
time: 2018-05-13T17:31:05-04:00
published_filename: 2018-05-13-a-first-post-on-wirebird.html
guid: 3BF68422-5727-11E8-A51C-E9E65B022F40

I've spent the last couple years or so digging into standards and 
playing around with Perl libraries and it's probably about time to 
buckle down, stop chasing the new hotness, and build some working code. 
So here's the plan.

As I've mentioned here and there, my goal is simultaneously absurdly 
unachievable and very modest. On the one hand, I want to destroy 
Facebook. 

![Paul Ford tweets: "...We have to destroy Facebook."](http://tilde.club/~silver/images/DestroyFacebook.png)

On the other hand, I just want a little standards-compliant blogging 
software that's easy enough for my mom to keep up with and comment on.

So here's the plan.

### An integrated feed reader and CMS

If you think about it, that's what Facebook is. That's what Twitter is. 
And so forth. It's a platform where you read stuff other people have 
written, and you write stuff.

### A client for walled gardens

This is tough because most of them have TOS's and API's that are 
explicitly designed to prevent silo-busting clients. But if it can do 
no more than *read* Facebook, for instance, that's still an 
improvement. Imagine being able to control what appears in your 
timeline!

Furthermore, though, it allows *owning your words* - you don't need to 
rely on Facebook's export function if your client has been maintaining 
a local copy of everything.

### A RESTful, standard interface

TL;DR: It will use machine-readable, extensible standards so clients 
can proliferate yet users don't have to wait for their favorite to 
update to the newest features.

The server will be structured around 
[http](https://tools.ietf.org/html/rfc2616), 
[SIOC](http://rdfs.org/sioc/spec/), and [schema](http://schema.org/) 
standards:

* Client reaches a page, sees the RDF/XML/JSON encodings, and chooses the flavor it likes best.
* Client sees that this is (for example) a schema:BlogPosting/sioc:BlogPost. Based on those standards, it can automatically offer the ability to post a sioc:Comment. ð _It offers that within its own, familiar-to-the-user UI._
* It POSTs a sioc:Comment to the sioc:BlogPost. The server examines it for standards compliance and accepts it (either directly or into the moderation queue).
  * Alternately, the client publishes the sioc:Comment on its own server, and simply POSTs the url. The commenter owns their words! (Of course, the server will probably want to maintain a local version to prevent bait-and-switch.)

Clients can (and should) customize their interface for posting 
different types of content, but in a worst-case scenario the 
machine-readable standards mean the "API" can change without breaking 
anything. For instance:

* Client encounters a [schema:BroadcastService](http://pending.schema.org/BroadcastService); this did not exist when the client was written, but it can look up the linked standard and recognize that, for instance, `broadcastAffiliateOf` is an `Organization` and display it accordingly, adding the clarification provided by the standard: `The media network(s) whose content is broadcast on this station.`
* Client recognizes that a broadcast service can contain a `BroadcastChannel`, and that http POST is allowed to this page, so it knows that it can offer the ability to add `A unique instance of a BroadcastService on a CableOrSatelliteService lineup.`
* Client doesn't know what a `BroadcastChannel` is, so it looks up the standard and knows that the form it offers its user should be a text field for `broadcastChannelId`. It can also offer the user the text explanation of what should go there: `The unique address by which the BroadcastService can be identified in a provider lineup. In US, this is typically a number.`
* Client can POST the new channel to the broadcast service. Based on the RFC-compliant HTTP response from the server, the client can tell the user whether the action succeeded or failed, and if the latter (hopefully) provide hints as to what changes need made.

The client might present form fields in a generic (alphabetic?) order, leaving it to the user to figure out which ones are most important, but it's better than nothing. (And perhaps there's a `wirebird:` standard where the client can look up templates for new standards, and apply them without having to do a software update.)

### ActivityPub compatibility

ActivityPub has a
[machine-readable](https://www.w3.org/TR/activitypub/)
[structure](https://www.w3.org/TR/activitystreams-vocabulary/), so it
will fit into the above interface, structurally. Layering the
ActivityPub API's onto it will allow federation with Mastodon,
Aardwolf, and [anything
else](https://activitypub.rocks/implementation-report/) that uses the
standard.


