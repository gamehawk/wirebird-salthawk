title: Preloading the database
time: 2018-05-16T11:04:46-04:00
published_filename: 2018-05-16-preloading-the-database.html
guid: 78F9BCFA-591A-11E8-B7BD-C26B12D66CC3

As I [mentioned](2018-05-14-building-a-minimal-feed-reader.html), to 
start with we'll just pre-populate the database rather than worrying 
about the interface up to that point.

[Birdfeeder.pl](https://gitlab.com/gamehawk/wirebird-salthawk/blob/master/script/Birdfeeder.pl)
is the script that will prepack the database. It doesn't *create* the
database; that's a generic Postgres database that has had
[RDF::Trine::Store::DBI](https://metacpan.org/pod/RDF::Trine::Store::DBI)
do a new() on. Which, yeah, should really be in either the script or in
Wirebird.pm itself.

Anyway, it's a hacky little script that does some very, very minimal
error catching and debugging, but until there's real validation
happening inside Wirebird.pm it really doesn't matter.

Birdfeeder makes a site root page, a usergroup page, a user, an inbox, 
an outbox, and a subscriptionlist. Since I don't have a front end 
going, the templates for those page types have some generic html forms 
on them. All we really care about right now is that the subscription 
page has a form where you can post an url to the subscription page. 
(The form could be anywhere, but KISS for now.)

The templates are pretty utilitarian, designed to give me as much debug 
info as possible rather than to look like the finished project. Over in 
the sidebar is the JSON representation of the data. Here's the site 
root:

![Screenshot of demo site root](2018-05-16_Website.png)

Again, for a single-user site this would probably be a control panel, 
or maybe even just the Inbox directly. This installation has 
auto-populated itself from the machine name 
([habrok](https://en.wikipedia.org/wiki/H%C3%A1br%C3%B3k)) and my 
username ([silver](https://duckduckgo.com/?q=%22mercedes+silver%22+%22battleship+grey%22)).

![Screenshot of demo site usergroup](2018-05-16_Usergroup.png)

A usergroup of one.

![Screenshot of demo site user](2018-05-16_User.png)

It leads to a lot of pointless nesting when every URI is an actual URL 
and there's only one user, but obviously you wouldn't need to navigate 
your way down; the top page can (just via templates) display the inbox 
or whatever else we want, and that's without even layering a Javascript 
client on top.

![Screenshot of demo site subscription page](2018-05-16_SubscriptionList.png)

Birdfeeder just directly calls the putPage() function, which gets 
called (after some authen and preliminary authz) by the Dancer app. If 
the validation was all in place, this would literally be all that's 
required for the minimum viability I described in the [first 
post](2018-05-13-a-first-post-on-wirebird.html): it's a server that 
servers and accepts structured JSON (only, so far) RESTfully. Granted, 
this offloads a *lot* onto the as-yet-imaginary client, but _there are 
standards, and they work._

(I mean, if they're properly implemented they work. Currently Wirebird 
cheerfully responds to that form submission with a simple `1` so 
obviously I should blog less, debug more.)

