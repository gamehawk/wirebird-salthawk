title: Rubberducking: performance versus completeness
time: 2018-06-06T19:48:11-04:00
published_filename: 2018-06-06-rubberducking-performance-versus-completeness.html
guid: 125641D2-69E4-11E8-BED2-2DEB0256252B

![XKCD 927: standards](https://imgs.xkcd.com/comics/standards.png)

As the Wirebird::Remote hierarchy gradually fills in, there are some 
things I need to consider. As I've alluded to, very often a resource 
will have [multiple sources of 
data](http://wirebird.com/2018-05-31-rubberducking-more-shadow-profiling.html) 
and Wirebird will need to decide what's best because there is no 
"authoritative" source. The `bid()` system currently just parses the 
best available source (in terms of what the standard _can_ provide, not 
necessarily what a particular implementation _does_ provide), but 
ultimately it will need to parse all sources above a minimum quality. 
For instance:

Back to my mastodon.social profile as an example. Currently, Mastodon 
can parse it as follows, in order of preference:

  * Atom
  * RSS
  * HTML

The RSS doesn't offer anything the Atom doesn't, but if the Atom is 
missing, the RSS doesn't have a lot of things the HTML does. But the 
standard offers it, so I don't want to necessarily bump RSS below HTML 
in priority. So it's probably going to be necessary for the 
`Remote::retrieve()` routines to not stomp on already-existing 
properties.

When processing the subscriptions, things get even more interesting. If 
we process the feed entries, that RSS still doesn't have authorship 
information - but if we follow the link to the page for each status we 
have a lot more options. That means a lot more http calls, though. 
Probably unavoidable - I just have to keep track of whether a link has 
been thoroughly mined out, so I don't keep calling it every time 
Wirebird notices it.

["Minimum viable product"](http://wirebird.com/2018-05-14-building-a-minimal-feed-reader.html) 
turns out not to be so "minimal" when you're trying to build a solid 
foundation.

_No commits for a couple days, sorry. I was wrong last week when I said 
I'd have more time this week._

