title: Cleaning house: testing everything
time: 2018-06-01T21:25:39-04:00
published_filename: 2018-06-01-cleaning-house-testing-everything.html
guid: DBD01C3E-6603-11E8-8B61-917CA11C8E6A

The house cleaning around here has been a little more literal; we're 
having some HVAC work done so it was time to make the place more 
presentable.

I've added Wirebird::Handler::Profile which starts to implement some of 
the shadow profiling I mentioned yesterday. But that caused some side 
effects: checking both the Atom and RSS feeds means my Mastodon account 
gets two different names, and the template populator doesn't cope well 
with that so it's time to revisit that.

Now, the reason the names are different is because one is 
double-encoded... and the other is triple-encoded. So next week I'll 
have to bite the bullet and get the Unicode stuff working consistently 
at all levels. (Not looking forward to that, especially dealing with 
older CPAN modules.)

The test suite is getting to the point where it needs to be broken up 
into multiple steps, which is good. The tests are still not terribly 
robust, but they're at least making sure I (probably) don't break old 
things while building new things.

So next week's plan:

  * Fix unicode
  * Strip HTML for plaintext fields
  * Fix template population
  * Decide on authen/authz model

In the unlikely event I get all that done (though I do expect to have 
more time next week than I had this one), I'll continue down the 
[list of things required to reach viability](http://wirebird.com/2018-05-30-coding-out-loud-approaching-minimum.html).

