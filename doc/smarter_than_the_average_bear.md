title: Smarter than the average bear
time: 2018-06-19T15:50:27-04:00
published_filename: 2018-06-19-smarter-than-the-average-bear.html
guid: 03724222-73FA-11E8-BCAE-B3B6E240A448

<iframe src="https://mastodon.xyz/@CarlCravens/100232088539418132/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mastodon.xyz/embed.js" async="async"></script>

Apparently one of my husband's co-workers thinks I'm the smarter one.

Joke's on him: I spent today failing to properly install mod_perl on a basic Apache install, then giving up and failing to even correctly activate a cgi-bin dir.

(Look, it's been awhile, okay?)

