title: Building tests and cleaning up
time: 2018-05-29T18:14:24-04:00
published_filename: 2018-05-29-building-tests-and-cleaning-up.html
guid: A4FD47C2-638D-11E8-9A8F-016EA11C8E6A

If you've been following the repo, you may have noticed the 
disappearance of Birdfeeder.pl, the script that builds the test 
database. All that has been rolled into a test script, which makes me 
feel a lot better about it. It's not really robust testing yet, but it 
at least does some very basic recapitulation of what's going on so far, 
and will let me know if anything obviously stops working.

Per yesterday's musing about [shadow 
profiles](2018-05-28-rubberducking-building-shadow-profiles.html) I 
just-for-fun decided to add an email address to the local-user profile 
it builds for me, and it faithfully matched up the blog and put my name 
in the byline. This should be no surprise to me, but it's still 
gratifying. One of the dev Slacks I'm in had a recent comment by a 
newbie developer about being surprised/excited when something trivial 
runs on the first try, and asking if that surprise/excitement ever wore 
off. The answer was, collectively, "a little but it still happens every 
now and then.

I also added my Mastodon account, as a regular feed. Masto provides 
both Atom and RSS, so just for fun I have decided to subscribe to both. 
Ideally, Wirebird will recognize that they're two feeds for the same 
site, but Mastodon uses different link formats so I end up with 
duplicate entries. They'll both canonicalize to the same base url *if* 
Wirebird retrieves the page instead of just the feed entry, so that's 
another point in favor of doing that. The Atom feed does include the 
RSS-style link as an alternate, so that's probably another thing 
Wirebird::Handler::XMLFeed should deal with... and a pretty good 
argument for splitting Atom and RSS handling, since XML::Feed flattens 
the two into least-common-denominator.

Mastodon also includes some very extensive profile information in the 
Atom feed, which is pretty neat. The rss feed has zero author 
information at all, which I guess makes it still useful for testing 
purposes.

