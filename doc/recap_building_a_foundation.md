title: Recap: building a foundation
time: 2018-06-12T17:35:17-04:00
published_filename: 2018-06-12-recap-building-a-foundation.html
guid: 7FAAE014-6E88-11E8-8948-61B801E0D2FF

It's been a month since I started the project, so it seems like a good 
place to pause and summarize where everything is. The goal is to 
[replace Facebook.](http://wirebird.com/2018-05-13-a-first-post-on-wirebird.html) 
The [minimum viable product](http://wirebird.com/2018-05-13-steps-toward-a-minimum-viable-product.html) 
is, first, a feed reader - whose most important feature is that a user 
doesn't *know* it's a feed reader. The user just "follows" people and 
organizations, just like on Facebook, and things magically appear on 
her feed/wall, just like on Facebook.

It's also standards-based, because there's no point in replacing 
Facebook with another silo. That doesn't entirely matter until the 
blogging side of things is in place, but the triplestore is there under 
the hood.

The [repo](https://gitlab.com/gamehawk/wirebird-salthawk) still doesn't 
really have a functioning app yet, but it's out there in the world 
mostly to keep me accountable.

At [one point,](http://wirebird.com/2018-05-30-coding-out-loud-approaching-minimum.html) 
the critter could actually read Atom and RSS feeds and put them in the 
triplestore (and the ActivityPub inbox), but I've taken that apart 
again and that's been my focus lately: a robust, extensible system to 
retrieve wild data and process it into proper Linked Data. Syndication 
feeds, being already XML, _should_ require the least processing but of 
course it [turns out](http://wirebird.com/2018-06-11-coding-out-loud-facepalm-emoji.html) 
that CPAN libraries are mostly geared toward getting those feeds 
*farther* from machine-readable instead.

So here's where I'm at on things it does and needs to do for MVP:

  * _read a basic bootstrap config file, or_ guess at basic defaults based on hostname, username, and a default permanent store
  * bootstrap a basic website
  * allow feeds to be added to _and deleted from_ the subscription list
  * __poll Atom/RSS/JSON/RDF feeds and add any new entries to the feed and to the subscriber’s inbox__
  * allow entries to be deleted from the inbox ("mark read")
  * _import (or export) OPML_
  * _serve inbox as linked data in JSON/Turtle/XML/etc. format_
  
Emphasized items are __incomplete__ or _nonexistent._

On a tangentially related note: I visited a [Philadelphia Perl Mongers](http://philadelphia.pm.org/) 
meeting last night, since [the topic was Linked Data](https://www.meetup.com/Philadelphia-Perl-Mongers/events/czslxlyxjbpb/). 
The speakers described building the [Global Change Information System](https://data.globalchange.gov/) 
which, of course, runs on something a little more powerful than a 
Raspberry Pi, and for an audience a little more tech-savvy than my 
hypothetical end users. It was still pretty interesting, if for no 
other reason than it was good to hear that somebody other than People 
In 2007 are actively developing these things, even if not in the 
particular ontologies I'm working with.

