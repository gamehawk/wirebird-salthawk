title: Subscribing to something
time: 2018-05-17T12:19:16-04:00
published_filename: 2018-05-17-subscribing-to-something.html
guid: 0BCE99D2-59EE-11E8-8ED8-A491A565BE5B

Subscribing to something is a pretty close analog to an [ActivityPub 
Follow](https://www.w3.org/TR/2018/REC-activitypub-20180123/#follow-activity-outbox), 
and I'm somewhat inclined to make it one. There are a couple of 
potential gotchas, though: an Actor's Following list is generally 
public, where a person's Atom/RSS subscriptions generally aren't. SIOC 
can handle this because there is no definition of how many 
SubscriptionLists a user can have. ActivityPub's spec doesn't seem to 
exclude multiple outboxes, but I wouldn't care to predict what a 
client's reaction to that might be.

Both SIOC and ActivityPub are pretty clear that a person can have 
multiple UserAccount/Actor objects linked to them, so that's probably 
where things should branch: "this is my UserAccount for feed reading; 
it's not visible to the public." Two UserAccounts can point to the same 
Inbox, so that would work fairly transparently to the user. For now, 
I'm not going to move it anywhere but I guess I'll have to keep it in 
mind.

No commit today; I only have three or four hours a day, tops, to put 
into Wirebird and I've been reading [Learning 
SPARQL](http://learningsparql.org/) rather than coding. I've gone back 
to the 
[tests](https://gitlab.com/gamehawk/wirebird-salthawk/tree/master/t) 
which, if you are looking at the current-as-I-write-this repo, don't at 
all match the direction Wirebird.pm took. Getting those up to date is 
particularly important to me because there are times I don't get to 
work on code at all for long stretches, and picking up on the last 
failing test is the only "to do list" I have found that works for me.

I also am thinking of a structural change: instead of passing around 
the $data (or $json as it is usually called in the code) hashref 
holding the current resource item, I may make it a full-blown object. 
Making the Wirebird object itself respresent the resource may or may 
not work (since sometimes it's working with a parent and child resource 
or the like) so I'm thinking of a Wirebird::Resource object, with the 
plugin system attached to that. So I want to let that simmer a bit 
before moving forward with code.

