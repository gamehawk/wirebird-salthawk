package Wirebird;

use strict;
use warnings;
use Carp;

use Net::Domain qw(hostname);
use DBI;
use RDF::Trine;
use RDF::NS;
use RDF::SN;
use RDF::Query;
use JSON;
use Path::Tiny;
use Scalar::Util qw/reftype blessed/;
use HTML::Template;
use XML::Feed;
use Data::UUID::Concise;
use HTML::Formatter;
use File::Slurp;
use match::simple qw(match);
use POSIX qw/strftime/;
use LWP::UserAgent;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($ERROR);

use Data::Dumper;

use Module::Pluggable
  require     => 1,
  sub_name    => '_handlers',
  search_path => ['Wirebird::Handler'];
our @HANDLERS;

sub handlers {
    if ( !@HANDLERS ) {
        @HANDLERS = shift->_handlers;
    }
    return @HANDLERS;
}

use Module::Pluggable
  require     => 1,
  sub_name    => '_pagetypes',
  search_path => ['Wirebird::Remote'];
our @PAGETYPES;

sub pagetypes {
    if ( !@PAGETYPES ) {
        @PAGETYPES = shift->_pagetypes;
    }
    return @PAGETYPES;
}

=head1 NAME

Wirebird - Core library for the Wirebird Semantic Web framework

=head1 VERSION

Version 0.01

=head1 SYNOPSIS

This module lives over a local Redland store, and performs all the
handy higher-level functions to process Semantic Web pages locally and
remotely.


=head1 DESCRIPTION

Wirebird is the base class for Wirebird instances.

=head1 INITIALIZATION

=head2 new ([server => $baseurl, ...])

Call new() to create a new Wirebird object:

	my $wirebird = Wirebird->new( server => 'you.local' );

Name => value pairs are optional, and map to the following setters.

=cut

sub new {
    my ( $class, %parms ) = @_;

    my $self = {};
    bless $self, $class;

    # TODO well %parms certainly needs some sanitization
    foreach my $key ( keys %parms ) {
        $self->$key( $parms{$key} );
    }
    return $self;
}

=head2 server ( [$baseurl] )

Sets base url that will be used to construct local urls. If not set,
defaults to a non-secure url built from Net::Domain::hostname and
port(), so you might end up with 'http://machinename:6809/'.

Note that this doesn't affect anything on the underlying system, so
your Wirebird installing might have a mismatch - serving up web pages
that point your browser to a different server.

=cut

sub server {
    my ( $self, $server ) = @_;

    # TODO need better secure detection
    # although maybe upstream
    if ( $self->port == 80 ) {
        $self->{'server'} =
          $server || $self->{'server'} || 'http://' . hostname . '/';
    }
    elsif ( $self->port == 443 ) {
        $self->{'server'} =
          $server || $self->{'server'} || 'https://' . hostname . '/';
    }
    else {
        $self->{'server'} =
             $server
          || $self->{'server'}
          || 'http://' . hostname . ':' . $self->port . '/';
    }
    return $self->{'server'};

}

=head2 port ( [$portnumber] )

Sets or returns the default port number that will be used to construct
local urls. If set to 80 or 443 the url constructed by server() will be
http or https respectively and the port number will be left out.

NB: Setting server() directly will override this! This is only used to
build the default url from hostname.

=cut

sub port {
    my ( $self, $port ) = @_;

    # non-arbitrary port selection :}
    $self->{'port'} = $port || $self->{'port'} || '6809';
    return $self->{'port'};
}

=head2 store ( [$storename, [RDF::Trine::Store->new(...)]] )

Set or returns an RDF store by the given name. Storename defaults to
'wbstore', the internal store.

NB: Currently the default store is always a Postgres database named
'wirebird' regardless of the store name. If you want to set up multiple
stores you will have to do so explicitly.

=cut

sub store {
    my ( $self, $storename, $store ) = @_;
    $storename ||= 'wbstore';
    $self->{'store'} ||= {};

    # TODO test for existence? below is carryover from sqlite
    #if ( !-f 'data/' . $storename . '.db' ) {
    #croak "No such store $storename\n";
    #}

    $self->{'store'}->{$storename} =
      $store || $self->{'store'}->{$storename} || RDF::Trine::Store->new(
        {
            storetype => 'DBI',
            name      => 'wb_main',    # todo based on hostname? storename?
            dsn => "dbi:SQLite:dbname=data/$storename.db",

            #dsn      => "dbi:Pg:dbname=wirebird",
            username => q{},
            password => q{},
        }
      );
    croak "No such store $storename " . $! if !$self->{'store'}->{$storename};

    # TODO test for valid connection
    return $self->{'store'}->{$storename};
}

=head2 model ( [$storename] )

Returns an RDF::Trine::Model corresponding to the store() of that name.
Defaults to 'wbstore'.

=cut

sub model {
    my ( $self, $modelname ) = @_;
    $modelname ||= 'wbstore';

    $self->{'model'} ||= {};

    # TODO test for existence
    $self->{'model'}->{$modelname} ||=
      RDF::Trine::Model->new( $self->store($modelname) );
    return $self->{'model'}->{$modelname};
}

=head2 ns ( [ $file_or_date ] [ %options ] )

Returns an RDF::NS object, with options passed directly to
RDF::NS->new. This will be used to expand namespace prefixes.

=cut

sub ns {
    my ( $self, @parms ) = @_;
    delete $self->{'ns'} if @parms;
    $self->{'ns'} ||= RDF::NS->new(@parms);
    return $self->{'ns'};
}

=head2 sn ( [ $file_or_date ] [ %options ] )

Returns an RDF::SN object, with options passed directly to
RDF::SN->new. This will be used to compress namespace prefixes.

=cut

sub sn {
    my ( $self, @parms ) = @_;
    delete $self->{'sn'} if @parms;
    $self->{'sn'} ||= RDF::SN->new(@parms);
    return $self->{'sn'};
}

=head1 FUNCTIONS

=head2 jsoncoder( )

Returns a JSON encoder object.

=cut

sub jsoncoder {
    my $self = shift;
    $self->{jsoncoder} ||=
      JSON->new->convert_blessed->ascii->pretty->allow_nonref;
    return $self->{jsoncoder};
}

=head2 userAgent( [$lwpuseragent] )

Sets or returns a LWP::UserAgent (or whatever drop-in substitute is
provided).

=cut

sub userAgent {
    my ( $self, $useragent ) = @_;
    $self->{'userAgent'} =
      $useragent || $self->{'userAgent'} || LWP::UserAgent->new;
    return $self->{'userAgent'};
}

=head2 shorten( $urlfragment )

Returns a UUID based on the url fragment and the base url. Used to
construct new urls after POST.

=cut

sub shorten {
    my $self = shift;
    $self->{du}  ||= Data::UUID->new();
    $self->{duc} ||= Data::UUID::Concise->new();
    return $self->{duc}
      ->encode( $self->{du}->create_from_name( $self->server, shift ) );
}

=head2 drop_resource( $url )

Drops all statements with given url as a subject. Brute-force before a
merge.

=cut

sub drop_resource {
    my ( $self, $url ) = @_;

    $self->model('wbstore')->remove_statements( iri($url), undef, undef );

    return $url;
}

=head2 isLocal( $url )

Identifies the provided url as being local. Very naive right now.

=cut

sub isLocal {
    my $self = shift;
    my $url  = shift;
    return substr( $url, 0, length( $self->server ) ) eq $self->server;
}

=head2 slugify($title, $fallback)

Super-basic slugification of a title.

=cut

sub slugify {
    my $self     = shift;
    my $title    = lc(shift);
    my $fallback = shift;
    return $fallback unless $title;
    $title =~ s/\W/\-/gx;
    $title =~ s/\-+/\-/gx;
    $title =~ s/^\-//x;
    $title =~ s/\-$//x;
    return $title || $fallback;
}

=head2 query ( $querystring[, $model] )

Performs the listed RDF::Query against the model (defaults to
$self->model), returning a listref of results (in hashref form).

=cut

sub query {
    my ( $self, $querystring, $model ) = @_;

    #print "Query: $querystring\n";

    $model ||= $self->model;

    my $query = RDF::Query->new( $querystring, { update => 1 } );
    if ( !$query ) {
        ERROR "Could not parse $querystring";
        return [];
    }
    my $iterator = $query->execute($model);
    my @rows;
    while ( my $row = $iterator->next ) {

        my $hash = {};
        foreach my $key ( sort keys %$row ) {
            if ( exists( $hash->{$key} ) ) {
                print "oops\n";
            }
            next unless $row->{$key};
            $hash->{$key} = $row->{$key}->value;
        }
        push @rows, $hash;
    }
    return \@rows;
}

1;

=head1 DIAGNOSTICS

=head1 CONFIGURATION AND ENVIRONMENT

=head1 DEPENDENCIES

TBA

=head1 INCOMPATIBILITIES

=head1 BUGS AND LIMITATIONS

So many bugs. Limited to my own setup right now.

=head1 AUTHOR

Karen Cravens (GAMEHAWK, @gamehawk@mastodon.social, tyrosinase @ Keybase)

=head1 LICENSE AND COPYRIGHT

No licensing yet. Certainly going to be Artistic/GPL (same-as-Perl) at minimum.

=head1 DISCLAIMER OF WARRANTY

The usual.

=cut
