package Wirebird::Handler::SubscriptionList;

use strict;
use warnings;

use Scalar::Util qw/reftype blessed/;
use RDF::TrineX::Functions -all;
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($ERROR);

use Data::Dumper;

=head2 bidCreate($parent, $child)

Returns the base priority() - that is, will bid based on whether the
parent is a SubscriptionList, regardless of what the child is.

=cut

sub bidCreate {
    my $class  = shift;
    my $parent = shift;
    my $child  = shift;
    return $class->priority($parent);
}

=head2 create($parent, $child, $parms)

Adds the child resource to the parent resource: gives parent and child
container_of/has_container links.

=cut

sub create {
    my ( $class, $parent, $child, $parms ) = @_;

    if ( $child->pageExists ) {
        DEBUG "Subscription page already exists";

        #		return $child;
    }
    my $base = $child->url;
    if ( !$child->wirebird->isLocal( $child->url ) ) {
        if (
            $child->wirebird->model->count_statements( iri( $child->url ),
                undef, undef ) > 0
          )
        {

            $child->set( curie('sioc:has_container'), iri( $parent->url ) );
            $parent->set( curie('sioc:container_of'), iri( $child->url ) );
            $child->status(201);
        }
    }
    else {
        print "Local url\n";
    }

# TODO when we actually do something, we must set the $child->status (or ->warnings/errors).
    return $child;
}

=head2 priority($resource)

Returns 100 if the given resource has a type of sioc:SubscriptionList.

=cut

sub priority {
    my $class    = shift;
    my $resource = shift;
    return 100 if $resource->hasType('sioc:SubscriptionList');
    return 0;
}

1;
