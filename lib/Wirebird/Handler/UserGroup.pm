package Wirebird::Handler::UserGroup;

use strict;
use warnings;

use Scalar::Util qw/reftype blessed/;
use RDF::TrineX::Functions -all;

=head2 bidCreate($parent, $child)

Returns the base priority() - that is, will bid based on whether the
parent is a SubscriptionList, regardless of what the child is.

=cut

sub bidCreate {
    my $class  = shift;
    my $parent = shift;
    my $child  = shift;
    return $class->priority($parent);
}

=head2 create($parent, $child, $parms)

Adds the child resource to the parent resource: adds all the parent's
subscribers to the child, and gives parent and child
container_of/has_container links.

In the future, when XMLFeed uses SPARQL instead of simple triples to
determine subscribership, the children will not need explicit
has_subscriber statements.

=cut

sub create {
    my ( $class, $parent, $child, $parms ) = @_;
    if ( !$child->url ) {

    }
    $child->model->add_statement(
        statement(
            iri( $child->url ),
            curie('sioc:member_of'),
            iri( $parent->url )
        )
    );
    $parent->model->add_statement(
        statement(
            iri( $parent->url ),
            curie('sioc:has_member'),
            iri( $child->url )
        )
    );
    $parent->wirebird->merge( $child->model );
    $parent->wirebird->merge( $parent->model );
    $child->changed();
    $parent->changed();

    # TODO trigger static file rebuilds
    print "Saved child as ", $child->url, "\n";
    return $child;
}

=head2 priority($resource)

Returns 100 if the given resource has a type of sioc:SubscriptionList.

=cut

sub priority {
    my $class    = shift;
    my $resource = shift;
    return 100 if $resource->hasType('sioc:UserGroup');
    return 0;
}

1;
