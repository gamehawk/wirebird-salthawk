package Wirebird::Handler::Profile;

use strict;
use warnings;

use Scalar::Util qw/reftype blessed/;
use RDF::TrineX::Functions -all;

=head2 shadow( $resource[, $reliability] )

Builds out the "shadow profile" for the person referred to by this 
Profile - i.e., retrieves all the remote information that can be 
inferred for them at a given reliability level (1-100, default 50).

Does not merge the resource's model into the permanent store; that is 
up to the caller.

=cut

sub shadow {
    my $class       = shift;
    my $resource    = shift;
    my $reliability = shift || 50;
    my %queue       = {};
    my $queued      = 0;
    do {
        # requeue
        foreach my $url ( keys %queue ) {
            next unless $queue{$url} eq 'new';

            my $newresource = Wirebird::Resource->new(
                wirebird => $resource->wirebird,
                url      => $url
            )->retrieve;
            if ( !$newresource ) {
                $queue{$url} = 'error';
                $queued--;
                next;
            }

            # TODO probably should do some sanity checking here
            my $iter =
              $newresource->model->get_statements( undef, undef, undef, undef );
            while ( my $row = $iter->next ) {

                #print join(', ', map {$_->value} @$row), "\n";
                $resource->model->add_statement($row);
            }
            $queue{$url} = 'done';
            $queued--;
        }
        foreach my $stmt (
            $resource->model->get_statements( undef, undef, undef )->get_all )
        {
            next if !$stmt->[2]->is_resource;
            next if exists $queue{ $stmt->[2]->value };

            # TODO make sure the predicate indicates this url is really us
            # tbh should probably be a positive match instead
            next if $stmt->[1]->value eq curie('rdf:type')->value;
            next if $stmt->[1]->value eq curie('rdf:Type')->value;
            next if $stmt->[1]->value =~ /_of$/x;    # skip member_of, etc.
            next if $stmt->[1]->value =~ /has_/x;    # skip has_follower, etc.
            print "Identity url? ", $stmt->[2]->value, "\n";
            $queue{ $stmt->[2]->value } = 'new';
            $queued++;
        }
    } until $queued == 0;

    #print "Result:\n", $resource->asTurtle, "\n";
    return $resource;
}

1;
