package Wirebird::Handler::Container;

use strict;
use warnings;

use Date::Parse qw/str2time/;
use POSIX qw/strftime/;
use RDF::TrineX::Functions -all;
use RDF::Query;

use Data::Dumper;

=head2 bidCreate($parent, $child)

Returns the base priority() - that is, will bid based on whether the
parent is an LDP Container, regardless of what the child is.

=cut

sub bidCreate {
    my $class  = shift;
    my $parent = shift;
    my $child  = shift;
    return $class->priority($parent);
}

=head2 create($parent, $child, $parms)

Adds the child resource to the as:item list in the parent resource.

Currently a placeholder - wraps the child in a Create regardless of its
type (including if it's an Action already).

Otherwise should generally be based on https://www.w3.org/TR/ldn/ and/or
https://www.w3.org/TR/2018/REC-activitypub-20180123/#delivery .

=cut

sub create {
    my ( $class, $parent, $child, $parms ) = @_;

    if ( $child->url ) {

        # existing submission

    }
    else {
        # new submission

    }

    # TODO location header?
    return $child;
}

=head2 priority($resource)

Returns 10 if the given resource is an LDP container.

=cut

sub priority {
    my $class    = shift;
    my $resource = shift;
    return 10
      if $resource->hasType('ldp:Container');
    return 0;
}

1;
