package Wirebird::Handler::Inbox;

use strict;
use warnings;

use Date::Parse qw/str2time/;
use POSIX qw/strftime/;
use RDF::TrineX::Functions -all;
use RDF::Query;

use Data::Dumper;

=head2 bidCreate($parent, $child)

Returns the base priority() - that is, will bid based on whether the
parent is an Inbox, regardless of what the child is.

=cut

sub bidCreate {
    my $class  = shift;
    my $parent = shift;
    my $child  = shift;
    return $class->priority($parent);
}

=head2 create($parent, $child, $parms)

Adds the child resource to the as:item list in the parent resource.

Currently a placeholder - wraps the child in a Create regardless of its
type (including if it's an Action already).

Otherwise should generally be based on https://www.w3.org/TR/ldn/ and/or
https://www.w3.org/TR/2018/REC-activitypub-20180123/#delivery .

=cut

sub create {
    my ( $class, $parent, $child, $parms ) = @_;

    if ( !$child->url ) {

        # this is a direct submission, hopefully
        # TODO this don't work yet obvs
        print "Child $child\n", Dumper($parms);
        die;
    }
    my $exists = $parent->wirebird->query(
        'PREFIX as: <' . $parent->wirebird->ns->as . '>
PREFIX atom: <' . $parent->wirebird->ns->atom . '>
PREFIX dct: <' . $parent->wirebird->ns->dct . '>
PREFIX rdf: <' . $parent->wirebird->ns->rdf . '>
SELECT *

WHERE {
	?first dct:isPartOf <' . $parent->url . '> .
	?first as:object <' . $child->url . '> .
}'
    );
    if ( scalar @$exists ) {
        print $child->url, " already seen in this inbox\n";
        return $exists->[0]->{'first'};
    }

    # TODO make sure it's an ActivityStreams item!
    if ( exists $parms->{model} ) {

        # fallback to older type
        foreach my $statement ( @{ $parms->{model} } ) {
            $child->add_statement( statement(@$statement) );
        }
    }
    my ($sequence) = @{
        $parent->wirebird->query(
            'PREFIX dct: <'
              . $parent->wirebird->ns->dct . '>
SELECT (count(?child) as ?count)
WHERE {	?child dct:isPartOf <' . $parent->url . '> . }'
        )
    };
    my $sortkey = sprintf( '%08d', $sequence->{'count'} || 0 );

    # TODO check to see if child is already an Action!

    my $wrapper = Wirebird::Resource->new(
        wirebird => $parent->wirebird,
        url      => $parent->url . '/' . $sortkey
    );
    $wrapper->set( 'rdf:type',     curie('as:Create') );
    $wrapper->set( 'as:summary',   literal('Item posted to Inbox') );
    $wrapper->set( 'as:object',    iri( $child->url ) );
    $wrapper->set( 'as:id',        iri( $child->url ) );
    $wrapper->set( 'dct:isPartOf', iri( $parent->url ) );
    $wrapper->set( 'dct:created',
        literal( strftime( '%Y-%m-%dT%H:%M:%SZ', gmtime() ) ) );

    if ( $sortkey == 0 ) {
        $parent->wirebird->model->remove_statements( iri( $parent->url ),
            curie('as:item'), curie('rdf:nil') );
        $parent->wirebird->model->add_statement(
            statement(
                iri( $parent->url ),
                curie('as:item'),
                iri( $wrapper->url . '#blank' )
            )
        );
        $parent->wirebird->model->add_statement(
            statement(
                iri( $wrapper->url . '#blank' ),
                curie('rdf:first'),
                iri( $wrapper->url )
            )
        );
        $parent->wirebird->model->add_statement(
            statement(
                iri( $wrapper->url . '#blank' ), curie('rdf:rest'),
                curie('rdf:nil')
            )
        );

        # 		my $update = $parent->wirebird->query(
        # 			'PREFIX as: <http://www.w3.org/ns/activitystreams#>
        # PREFIX rdf: <https://www.w3.org/TR/rdf-schema/>
        # #DELETE DATA { <' . $parent->url . '> as:item rdf:nil }
        # INSERT DATA {
        #   <' . $wrapper->url . '#blank> rdf:first <' . $wrapper->url . '> ;
        #        rdf:rest rdf:nil .
        #   <' . $parent->url . '> as:item <' . $wrapper->url . '#blank> .
        # }
        # '
        # 		) or croak('Query failed');
    }
    else {
        my $update = $parent->wirebird->query(
            'PREFIX rdf: <' . $parent->wirebird->ns->rdf . '>
PREFIX dct: <' . $parent->wirebird->ns->dct . '>

# INSERT {
#   ?tail rdf:rest <' . $wrapper->url . '#blank> .
#   <' . $wrapper->url . '#blank> rdf:first <' . $wrapper->url . '> ;
#        rdf:rest rdf:nil .
# }
# DELETE { ?tail rdf:rest rdf:nil . }
SELECT *
WHERE {
	?wrapper dct:isPartOf <' . $parent->url . '> .
	?tail rdf:first ?wrapper ;
	       rdf:rest rdf:nil .
}'
        );
        $parent->wirebird->model->remove_statements(
            iri( $update->[0]->{'tail'} ),
            curie('rdf:rest'), curie('rdf:nil') );
        $parent->wirebird->model->add_statement(
            statement(
                iri( $update->[0]->{'tail'} ),
                curie('rdf:rest'),
                iri( $wrapper->url . '#blank' )
            )
        );
        $parent->wirebird->model->add_statement(
            statement(
                iri( $wrapper->url . '#blank' ),
                curie('rdf:first'),
                iri( $wrapper->url )
            )
        );
        $parent->wirebird->model->add_statement(
            statement(
                iri( $wrapper->url . '#blank' ), curie('rdf:rest'),
                curie('rdf:nil')
            )
        );

    }
    $parent->wirebird->model->remove_statements( iri( $parent->url ),
        curie('as:totalItems'), undef );
    $parent->wirebird->model->add_statement(
        statement(
            iri( $parent->url ),
            curie('as:totalItems'),
            literal(
                $parent->model->count_statements(
                    undef, curie('dct:isPartOf'), iri( $parent->url )
                )
            )
        )
    );

    $wrapper->rewrite();
    $parent->rewrite();
    $child->rewrite();
    $child->status(301);

    # TODO location header?
    return $wrapper;
}

=head2 priority($resource)

Returns 100 if the given resource is the object of any as:inbox
(ActivityStreams inbox) predicates.

=cut

sub priority {
    my $class    = shift;
    my $resource = shift;
    return 100
      if map { $_->[2]->value }
      $resource->wirebird->model->get_statements( undef, curie('as:inbox'),
        iri( $resource->url ) )->get_all;
    return 0;
}

=head2 asHtml($resource[, $template[, $page]])

Returns the populated template for the resource. Defaults to the
"inbox" template, and page 1.

=cut

sub asHtml {
    my ( $class, $resource, $template, $page ) = @_;
    $template ||= $resource->template('inbox');
    $page ||= 1;

# TODO this is gonna give us duplicate results for entries with multiple authors, isn't it?
    my $items = $resource->wirebird->query(
        'PREFIX sioc: <' . $resource->wirebird->ns->sioc . '>
PREFIX as: <' . $resource->wirebird->ns->as . '>
PREFIX atom: <' . $resource->wirebird->ns->atom . '>
PREFIX dct: <' . $resource->wirebird->ns->dct . '>
PREFIX og: <' . $resource->wirebird->ns->og . '>
PREFIX rel: <' . $resource->wirebird->ns->rel . '>
PREFIX rdf: <' . $resource->wirebird->ns->rdf . '>
SELECT *
WHERE {
	?wrapper dct:isPartOf <' . $resource->url . '> .
	FILTER NOT EXISTS {?wrapper as:deleted ?deleted }
	?wrapper as:object ?url .
	?url atom:title ?name ;
		atom:published ?published ;
		atom:content ?content ;
		dct:isPartOf ?parent .
	OPTIONAL { ?url rel:enclosure ?attachment . }
	OPTIONAL { ?parent atom:author ?attributedTo . }
	OPTIONAL { ?parent atom:author/atom:name ?attributedToName . }
	OPTIONAL { ?url og:image ?image . }
}
ORDER BY ?published
limit 20 offset ' . ( $page - 1 ) * 20
    );

# technically this could be merged into the previous query and used with tmpl_if __first__
# That feels less efficient though. Might have TODO some performance testing on it.
    my $boxowner = $resource->wirebird->query(
        'PREFIX sioc: <' . $resource->wirebird->ns->sioc . '>
PREFIX as: <' . $resource->wirebird->ns->as . '>
PREFIX atom: <' . $resource->wirebird->ns->atom . '>
PREFIX dct: <' . $resource->wirebird->ns->dct . '>
PREFIX og: <' . $resource->wirebird->ns->og . '>
PREFIX rel: <' . $resource->wirebird->ns->rel . '>
SELECT *
WHERE {
	<' . $resource->url . '> sioc:has_owner ?url .
	?url sioc:name ?name .
	OPTIONAL { ?url dct:created ?created . }
	OPTIONAL { ?url sioc:about ?about . }
	OPTIONAL { ?url sioc:avatar ?avatar . }
}'
    );

    $template->param( 'items', $items );
    $template->param( 'owner', $boxowner );

    # $template->param(
    # 	'json',
    # 	$resource->wirebird->jsoncoder->encode(
    # 		{ owner => $boxowner, items => $items }
    # 	)
    # );

    # TODO: retrieve external scripts like templates
    $template->param(
        'script',
        [
            {
                script => '<script>$(".mark_read").submit(function(e) {
	var frm = $(this);

    $.ajax({
           type: "DELETE",
           url: frm.attr("action"),
           success: function(data)
           {
			   frm.closest("div").hide("slow");

           }
         });
    e.preventDefault();
});</script>'
            }
        ]
    );
    return $template;
}

1;
