package Wirebird::Remote::XML;

use strict;
use warnings;

use XML::LibXML;
use RDF::Trine::Model;
use RDF::TrineX::Functions qw< iri literal statement >
  ;    # curie is overridden below
use URI;
use match::simple qw(match);
use Scalar::Util qw/reftype/;
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($ERROR);

use Data::Dumper;

my $ns = {};

=head2 bid ($response)

Returns priority claim of 100 if response's content can be successfully
parsed as an XML::Feed.

=cut

sub bid {
    my $class    = shift;
    my $response = shift;
    return 50 if $response->content_type =~ /xml/x;
    return 0;
}

=head2 retrieve ($resource, $response)

Parses the response as an XML feed and sets the resource's triples
accordingly. Also sets the resource->url to the feed's base - the
current feed url is stored in the sioc:feed proprity, but since that
may change if the target site changes software it's the feed's base
that is considered canonical.

Currently sets the resource to a sioc:WebLog, but may take a closer
look in the future to see what kind of feed it really is.

Does *not* parse the feed's entries, only the feed information.

=cut

sub retrieve {
    my $class    = shift;
    my $resource = shift;
    my $response = shift;
    if ( exists $response->{'_wb_asrdfa'}
        && $response->{'_wb_asrdfa'}->graph->size )
    {
        # overridden
        #return $response->{'_wb_asrdfa'}->graph;
    }

    # TODO cached by bid() ?
    $response->{'_wb_asxml'} ||= XML::LibXML->load_xml(
        string     => join( "\n", $response->decoded_content ),
        URI        => $resource->url,
        no_blanks  => 1,
        no_cdata   => 1,
        no_basefix => 1
    );
    my $dom   = $response->{'_wb_asxml'};
    my $model = RDF::Trine::Model->new();

    #$dom->setURI( $resource->url );
    my $attrib = $dom->documentElement()->attributes();
    for ( my $i = 0 ; $i < $attrib->length() ; $i++ ) {
        my ( $isprefix, $prefix ) = split( ':', $attrib->item($i)->nodeName() );
        if ( $isprefix eq 'xmlns' ) {
            $prefix ||= 'default';
            $ns->{$prefix} = $attrib->item($i)->nodeValue();
            if ( $ns->{$prefix} =~ /\w$/x ) {
                $ns->{$prefix} .= '/';
            }
        }
    }

    # TODO this needs replaced with GRDDL processing someday
    my $nodehash = {};
    my $unstored = [];
    if ( $dom->documentElement()->nodeName eq 'rss' ) {
        $ns->{'default'} ||= 'http://webfeeds.org/rss/1.0';
        ( $nodehash, $unstored ) =
          _nodehash( $dom->getElementsByTagName('channel') );
    }
    elsif ( $dom->documentElement()->nodeName eq 'feed' ) {
        $ns->{'default'} ||= 'http://www.w3.org/2005/Atom';
        ( $nodehash, $unstored ) =
          _nodehash( $dom->getElementsByTagName('feed') );
    }
    else {
        ERROR "Unknown XML type. " . $dom->documentElement->nodeName;
        ( $nodehash, $unstored ) = _nodehash( $dom->documentElement() );
    }
    _hashstore( $nodehash, $model );
    my $subresources = [];
    foreach my $subnode (@$unstored) {
        my ( $hashagain, $unstagain ) = _nodehash($subnode);

       # TODO perform a Handler::Foo->create against the top-level node?
       # my $bid = {};
       # foreach my $handler ( $resource->wirebird->handlers ) {
       # 	if ( $handler->can('bidCreate') ) {
       # 		my $priority = $handler->bidCreate( $resource, $subresources->[-1] );
       # 		$bid->{$handler} = $priority if $priority;
       # 	}
       # }
       # foreach my $handler ( sort { $bid->{$b} <=> $bid->{$a} } keys %$bid ) {
       # 	TRACE "Can be handled by $handler";
       # }
        if ( $subnode->nodeName() eq 'author' ) {
            my $suburl = _hashstore( $hashagain, $model );
            $model->add_statement(
                statement(
                    iri( $resource->url ), curie('atom:author'),
                    iri($suburl)
                )
            );
            next;
        }
        my $submodel = RDF::Trine::Model->new();
        push @$subresources, $submodel;
        my $suburl = _hashstore( $hashagain, $submodel );
        $subresources->[-1]->add_statement(
            statement(
                iri($suburl), curie('dcterms:isPartOf'),
                iri( $resource->url )
            )
        );
    }

    $resource->children($subresources);    # save for later use
    return $model;
}

sub _nodehash {
    my ($node)   = @_;
    my $hash     = {};
    my $unstored = [];

    foreach my $subnode ( $node->nonBlankChildNodes() ) {
        my $stored = 0;
        if ( $subnode->hasAttributes ) {
            my $attrib  = $subnode->attributes();
            my $subattr = {};
            for ( my $i = 0 ; $i < $attrib->length() ; $i++ ) {
                $subattr->{ $attrib->item($i)->nodeName() } =
                  $attrib->item($i)->nodeValue();
            }
            if ( $subnode->localname() eq 'link' ) {

                # https://www.w3.org/TR/html5/links.html#link-type-alternate
                if ( exists $subattr->{'rel'}
                    && $subattr->{'rel'} =~ /stylesheet/ )
                {

                    # ignore stylesheets
                    $stored++;
                }
                elsif (
                       exists $subattr->{'rel'}
                    && ( $subattr->{'rel'} eq 'alternate' )
                    && exists $subattr->{'type'}
                    && (   $subattr->{'type'} eq 'application/rss+xml'
                        || $subattr->{'type'} eq 'application/atom+xml' )
                  )
                {
                    _coru( $hash, 'sioc:feed', $subattr->{'href'} );
                    $stored++;
                }

# elsif (exists $subattr->{'rel'} && ($subattr->{'rel'} eq 'alternate') && exists $subattr->{'type'} && ($subattr->{'type'} eq 'text/html')) {
# 	# TODO
# 	DEBUG "REVERSE FEED LINK FOUND: ", $subattr->{'href'};
# 	#_coru($hash, 'sioc:feed_of', $subattr->{'href'});
# 	$stored++;
# }
                elsif ( exists $subattr->{'rel'} ) {
                    _coru( $hash, 'rel:' . $subattr->{'rel'},
                        $subattr->{'href'} );
                    $stored++;
                }
                elsif ( exists $subattr->{'rev'} ) {

                    # TODO
                    DEBUG "REVERSE LINK FOUND: ", $subattr->{'href'};

                  #_coru($hash, 'rev:' . $subattr->{'rev'}, $subattr->{'href'});
                    $stored++;
                }
                elsif ( $attrib->length() == 1 && exists $subattr->{'href'} ) {

                    #TRACE "Assuming rel:self";
                    _coru( $hash, 'rel:self', $subattr->{'href'} );
                    $stored++;

                }
                else {
                    DEBUG "Unknown attributes for ", $subnode->nodeName(),
                      ': ', join( ', ', keys %$subattr );
                }
            }
            elsif ( scalar( keys %$subattr ) == 1 ) {
                if ( exists $subattr->{'type'} ) {

                    # TODO store type somewhere?
                    DEBUG "Dropping attribute ", $attrib->item(0)->nodeName(),
                      '=', $attrib->item(0)->nodeValue(), " for ",
                      $subnode->nodeName;
                }
                else {
#DEBUG "Using attribute ", $attrib->item(0)->nodeName(), '=', $attrib->item(0)->nodeValue(), " as value for ", $subnode->nodeName;
                    _coru( $hash, $subnode->nodeName(),
                        $attrib->item(0)->nodeValue() );
                }
                $stored++;
            }
            else {
                if ( exists $subattr->{'href'} ) {

                    # TODO store type somewhere?
                    _coru( $hash, $subnode->nodeName(), $subattr->{'href'} );
                }
                DEBUG "Unknown attributes for ", $subnode->nodeName(), ': ',
                  join( ', ', keys %$subattr );
            }
        }
        foreach my $subsubnode ( $subnode->nonBlankChildNodes() ) {
            if ( $subsubnode->nodeType == 3 || $subsubnode->nodeType == 4 ) {
                _coru( $hash, $subnode->nodeName(), $subsubnode->nodeValue() );
                $stored++;
            }
        }
        if ( !$stored ) {
            push @$unstored, $subnode;
        }
    }
    return $hash, $unstored;
}

sub _coru {    # create or update
    my ( $hash, $key, $value ) = @_;

    #TRACE "coru ", Dumper(@_);
    if ( !exists $hash->{$key} ) {
        $hash->{$key} = $value;
        return $hash->{$key};
    }
    if ( reftype $hash->{$key} ) {
        if ( !match( $key, [ keys %$hash ] ) ) {
            push @{ $hash->{$key} }, $value;
        }
        return $hash->{$key};
    }
    if ( $hash->{$key} eq $value ) {
        return $hash->{$key};
    }
    $hash->{$key} = [ $hash->{$key}, $value ];
    return $hash->{$key};
}

sub _hashstore {
    my ( $nodehash, $model, $url ) = @_;
    if ( !$url ) {
        $url =
             $nodehash->{'uri'}
          || $nodehash->{'url'}
          || $nodehash->{'id'}
          || $nodehash->{'rel:self'}
          || $nodehash->{'id'}
          || $nodehash->{'link'};
        if ( !$url && exists $nodehash->{'email'} ) {
            $url = 'mailto:' . $nodehash->{'email'};
        }
    }
    if ( !$url ) {
        DEBUG "Cannot store hash (no link): ", Dumper($nodehash);
        return;
    }
    foreach my $key ( keys %$nodehash ) {
        my $pred = $key;
        my $obj  = $nodehash->{$key};
        if ( URI->new($pred)->has_recognized_scheme ) {
            $pred = iri($pred);
        }
        else {
            $pred = curie($pred) || literal($pred);
        }
        if ( !reftype $obj) {
            $obj = [$obj];
        }
        foreach my $objloop (@$obj) {

            if ( URI->new($objloop)->has_recognized_scheme ) {
                $model->add_statement(
                    statement( iri($url), $pred, iri($objloop) ) );
            }
            else {
                $model->add_statement(
                    statement( iri($url), $pred, literal($objloop) ) );
            }
        }
    }
    return $url;
}

=head2 getSubscribers( $resource )

Returns all the inbox resources for subscribers to the resource
(currently only explicit has_subscriber properties, but eventually will
use a proper SPARQL search).

=cut

sub getSubscribers {
    my $class    = shift;
    my $resource = shift;

    my @inboxes = map {
        Wirebird::Resource->new(
            wirebird => $resource->wirebird,
            url      => $_->[2]->value
          )
      }
      map {
        $resource->wirebird->model->get_statements( iri( $_->[2] ),
            curie('as:inbox'), undef )->get_all
      } $resource->model->get_statements( undef, curie('sioc:has_subscriber'),
        undef )->get_all;
    return \@inboxes;
}

=head2 pollSubscription($resource)

If the resource has any subscribers (currently only explicit
has_subscriber properties, but eventually will use a proper SPARQL
search), retrieves the current feed(s) for the resource, iterates over
the entries, and adds any new ones to the store and links them into
subscribers' inboxes.

Currently builds the entry resources from the XML::Feed::Entry, but
maybe someday will attempt its own Wirebird::Remote retrieval of the
actual pages.

=cut

sub pollSubscription {
    my ( $class, $resource, $inboxes ) = @_;

    $inboxes ||= [];
    my $success = 0;
    DEBUG "Inboxes ", join( ', ', @$inboxes );
    $resource->retrieve;

    # TODO sort by date!
    foreach my $model ( sort _sortPollSubscription @{ $resource->children } ) {
        my ($url) = $model->subjects( undef, undef );
        my $child = Wirebird::Resource->new(
            wirebird => $resource->wirebird,
            url      => $url->value
        );
        if ( $child->pageExists ) {
            DEBUG "Entry for ", $child->url, " has been seen before";
        }
        foreach my $statement (
            $model->get_statements( undef, undef, undef )->get_all )
        {
            $child->model->add_statement($statement);
        }
        $child->retrieve();    # pull resource proper
                               # TODO pull author from parent resource as needed
        DEBUG $resource->url, "\n", $child->asTurtle;

        #$child->wirebird->merge( $child->model );
        foreach my $inbox (@$inboxes) {
            DEBUG "Creating inbox entry";
            $inbox->create( $child, [] );
        }
    }

    #TODO in progress...
    return 1;
}

sub _sortPollSubscription {
    my ($adate) =
      $a->get_statements( undef, curie('atom:published'), undef )->get_all;
    my ($bdate) =
      $b->get_statements( undef, curie('atom:published'), undef )->get_all;

    $bdate->[2]->as_string cmp $adate->[2]->as_string;
}

=head2 curie( $value )

As the RDF::TrineX::Functions version (which it falls through to), but
matched against the $ns hashref that retrieve() populates from the
parsed XML file.

=cut

sub curie {
    my $value = shift;
    if ( URI->new($value)->has_recognized_scheme ) {
        return iri($value);
    }
    my ( $prefix, $local ) = split( ':', $value );
    if ( !$local ) {

        # no prefix
        if ( $ns->{'default'} ) {
            return iri( $ns->{'default'} . $prefix );
        }
        ERROR "NO PREFIX, NO DEFAULT";
        return literal($prefix);
    }
    if ( $ns->{$prefix} ) {
        return iri( $ns->{$prefix} . $local );
    }

    # fall back to default prefixes because somebody didn't declare theirs
    return RDF::TrineX::Functions::curie($value);
}

#~ XML_ELEMENT_NODE            => 1
#~ XML_ATTRIBUTE_NODE          => 2
#~ XML_TEXT_NODE               => 3
#~ XML_CDATA_SECTION_NODE      => 4
#~ XML_ENTITY_REF_NODE         => 5
#~ XML_ENTITY_NODE             => 6
#~ XML_PI_NODE                 => 7
#~ XML_COMMENT_NODE            => 8
#~ XML_DOCUMENT_NODE           => 9
#~ XML_DOCUMENT_TYPE_NODE      => 10
#~ XML_DOCUMENT_FRAG_NODE      => 11
#~ XML_NOTATION_NODE           => 12
#~ XML_HTML_DOCUMENT_NODE      => 13
#~ XML_DTD_NODE                => 14
#~ XML_ELEMENT_DECL            => 15
#~ XML_ATTRIBUTE_DECL          => 16
#~ XML_ENTITY_DECL             => 17
#~ XML_NAMESPACE_DECL          => 18
#~ XML_XINCLUDE_START          => 19
#~ XML_XINCLUDE_END            => 20

1;
