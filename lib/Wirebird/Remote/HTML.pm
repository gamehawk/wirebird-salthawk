package Wirebird::Remote::HTML;

use strict;
use warnings;

use RDF::TrineX::Functions -all;
use HTML::TreeBuilder;

=head2 bid ($response)

Returns priority claim of 1 if response's content_type is text/html.

We are not enthusiastic about this, because hopefully something more
specific will come along.

=cut

sub bid {
    my $class    = shift;
    my $response = shift;

    return 1 if $response->content_type eq 'text/html';

    return 0;
}

=head2 retrieve ($resource, $response)

Parses the response as a basic HTML page and sets the resource's
triples accordingly.

=cut

sub retrieve {
    my $class    = shift;
    my $resource = shift;
    my $response = shift;

    # TODO what an ugly way to cache this
    my $tree = $response->{'_wb_ashtml'};
    if ( !$tree ) {
        $tree = HTML::TreeBuilder->new;
        foreach my $line ( $response->decoded_content ) {
            $tree->parse($line);
        }
        $response->{'_wb_ashtml'} = $tree;
    }
    my $data = { 'as:url' => iri( $resource->url ) };

    #$data->{'rdf:type'} = curie('schema:WebPage');
    #$data->{'sioc:name'} = literal($tree->find_by_tag_name('title')->as_text);
    my @metas = $tree->find_by_tag_name('meta');
    foreach my $meta (@metas) {
        my $content = $meta->attr('content');

        # TODO fix encoding as necessary
        if ( $meta->attr('name') && $meta->attr('name') eq 'description' ) {
            $data->{'dct:description'} = literal($content);
        }

        # TODO multiple values will need to become arrays
        if ( $meta->attr('property') ) {
            if ( $meta->attr('property') !~ /^\w+\:\w+$/x ) {
                print "-- Confusing property: ", $meta->attr('property'), "\n";
                next;
            }
            if ( URI->new( $meta->attr('content') )->has_recognized_scheme ) {
                $data->{ $meta->attr('property') } =
                  iri( $meta->attr('content') );
            }
            else {
                $data->{ $meta->attr('property') } = literal($content);
            }
        }
    }
    my @links = $tree->find_by_tag_name('link');
    foreach my $link (@links) {

        # TODO rel=salmon
        if ( $link->attr('rel') eq 'alternate' ) {
            if ( $link->attr('type') =~ /atom/x ) {
                $data->{'sioc:feed'} = iri( $link->attr('href') );
            }
            elsif ( $link->attr('type') =~ 'rss' ) {
                $data->{'sioc:link'} ||= iri( $link->attr('href') )
                  ;    # don't override the atom if we found it
            }

            #			elsif ($link->attr('type') =~ 'json') {
            else {
                print "-- TODO alternate type: ", $link->attr('type'), "\n";
            }
        }
    }
    my $model = RDF::Trine::Model->new( RDF::Trine::Store::Memory->new() );
    foreach my $key ( keys %$data ) {

        #		print $key, ' ';
        #		print $data->{$key}->value, "\n";
        if ( !curie($key) ) {
            print "-- Bad prefix: $key\n";
            next;
        }
        $model->add_statement(
            statement( iri( $resource->url ), curie($key), $data->{$key} ) );
    }
    return $model;
}

1;
