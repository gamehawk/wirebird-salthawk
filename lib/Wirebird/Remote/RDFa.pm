package Wirebird::Remote::RDFa;

use strict;
use warnings;

use RDF::RDFa::Parser;
use RDF::TrineX::Functions -all;

=head2 bid ($response)

Returns priority claim of 50 if response's content contains RDFa data.

=cut

sub bid {
    return 0;    # TODO need to fix retrieval's blank-node Atom entries first
    my $class    = shift;
    my $response = shift;
    if ( $response->content_type =~ /[ht|x]ml/x ) {

        # TODO what an ugly way to cache this
        $response->{'_wb_asrdfa'} ||=
          RDF::RDFa::Parser->new_from_response($response);
        return 70 if $response->{'_wb_asrdfa'}->graph->size;
        return 70 if $response->{'_wb_asrdfa'}->opengraph;
    }
    return 0;
}

=head2 retrieve ($resource, $response)

Parses the response as an RDFa file and sets the resource's triples
accordingly.

=cut

sub retrieve {
    my ( $class, $resource, $response ) = @_;

    print "CALLING UNFINISHED LIBRARY REMOTE::RDFa\n";
    my $model = RDF::Trine::Model->new( RDF::Trine::Store::Memory->new() );

    # TODO cached by bid();
    my $page = $response->{'_wb_asrdfa'}
      || RDF::RDFa::Parser->new_from_response($response);

    $resource->url( $page->uri );
    if ( $resource->pageExists ) {

        # in case the new, corrected url exists
        $resource->status(200);

        #return $resource;
    }

    #TODO better response code
    $resource->status( $response->code );
    return $response->{'_wb_asrdfa'}->graph;
}

1;
