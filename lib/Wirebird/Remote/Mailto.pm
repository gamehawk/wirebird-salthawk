package Wirebird::Remote::Mailto;

use strict;
use warnings;

use RDF::TrineX::Functions -all;
use Email::Valid;
use DateTime;

=head2 retrieve ($resource, $uri)

Unlike http-flavored Wirebird::Remotes, expects a URI(.pm) object for a
mailto: uri instead of a HTTP::Response. Checks address for validity,
but not deliverability.

Sets the resource to a sioc:UserAccount with a sioc:email of itself.

May convert the email address into http://hostname/username (or
~username or @username) and check for the existence of those pages at
some point in the future.

=cut

sub retrieve {
    my $class    = shift;
    my $resource = shift;
    my $uri      = shift;
    if ( $uri->has_recognized_scheme ) {
        return if ( $uri->scheme ne 'mailto' );
    }
    else {
        $uri->scheme('mailto');
    }

    # see if we're the email address for an existing resource
    # TODO maybe deal with multiples, and/or check for more specific predicates?
    my ($hasParent) =
      map { $_->[0]->value }
      $resource->wirebird->model->get_statements( undef, undef,
        iri( $uri->as_string ) )->get_all;
    if ($hasParent) {
        $resource->url($hasParent);
        return $resource;
    }

    return if !Email::Valid->address( $uri->path );

# TODO look at http://hostname/username and /~username and /@username and webfinger and so forth and so on
    my $model = RDF::Trine::Model->new( RDF::Trine::Store::Memory->new() );
    $model->add_statement(
        statement(
            iri( $uri->as_iri ), curie('rdf:type'),
            curie('sioc:UserAccount')
        )
    );
    $model->add_statement(
        statement(
            iri( $uri->as_iri ),
            curie('dct:created'),
            literal( DateTime->now()->iso8601() . 'Z' )
        )
    );
    $model->add_statement(
        statement(
            iri( $uri->as_iri ),
            curie('dct:modified'),
            literal( DateTime->now()->iso8601() . 'Z' )
        )
    );
    $model->add_statement(
        statement(
            iri( $uri->as_iri ),
            curie('sioc:email'),
            iri( $uri->as_iri )
        )
    );
    return $model;
}

1;
