package Wirebird::Resource;

use strict;
use warnings;
use Carp;

use Scalar::Util qw/reftype blessed/;
use match::simple qw(match);
use Path::Tiny;
use File::Slurp;
use JSON;
use HTTP::Request;
use HTTP::Headers;
use POSIX qw/strftime/;
use RDF::TrineX::Functions qw< iri curie literal statement >;
use Encode;
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($ERROR);

use Data::Dumper;

# base class for Wirebird resources

=head2 new ([server => $baseurl, ...])

Call new() to create a new linked-data resource:

	my $resource = Wirebird::Resource->new( wirebird => $wirebird );

Name => value pairs are optional; if no Wirebird object is passed then
one will be created (which is not very efficient in most cases).

=cut

sub new {
    my ( $class, %parms ) = @_;

    my $self = {};
    bless $self, $class;

    # TODO well %parms certainly needs some sanitization
    foreach my $key ( keys %parms ) {
        $self->$key( $parms{$key} );
    }
    return $self;
}

=head2 url ( $url )

Sets or returns the local url for the resource.

=cut

sub url {
    my ( $self, $url ) = @_;

    if ($url) {
        if (   exists $self->{'url'}
            && exists $self->{'model'}
            && $self->{'url'} =~ /\/tmp\//x )
        {
            ERROR "Relocating not supported yet!";
        }
        else {
            $self->{'url'} = $url;
            delete $self->{'filename'};    #
        }
    }
    if ( !$self->{'url'} && $self->{'model'} ) {
        $self->{'url'} = $self->model->[0]->[0]->value;    # pretty brute-force
    }
    $self->{'url'} ||= q{};
    return $self->{'url'};

}

=head2 filename( [$filename] )

Sets or returns the filename (minus extension) for the static versions
of the resource.

By default, strips the base url and any file extension from the local
url to build the location.

=cut

sub filename {
    my ( $self, $filename ) = @_;

    if ($filename) {
        $self->{'filename'} = $filename;
    }
    elsif ( !exists $self->{'filename'} ) {
        my $url = $self->url;
        if ( $self->wirebird->isLocal($url) ) {
            $url = substr( $url, length( $self->wirebird->server ) );
            $url =~ s/\#(.*?)$//x
              ;    # probably should use URI and File::Spec and whatnot
            $url =~ s/\?(.*?)$//x;
            $url =~ s/\.(\w+)$//x;    # no extension
            $url =~ s/\/$/\/index/x;
            $url =~ s/^\///x;         # no leading slash
            $self->{'filename'} = $url;
        }
        else {
            $self->{'filename'} = 'cache/' . $self->wirebird->shorten($url);
        }
    }
    return $self->{'filename'};
}

=head2 wirebird ( [$wirebird] )

Sets the Wirebird object that will be used. If not set, defaults to a
generic one which may not be what you want and, if you're dealing with
multiple resources, may not be very efficient.

=cut

sub wirebird {
    my ( $self, $hashref ) = @_;

    if ($hashref) {
        $self->{'wirebird'} = $hashref;
    }
    $self->{'wirebird'} ||= Wirebird->new();
    return $self->{'wirebird'};
}

=head2 model ( [$model] )

Sets or returns the current working model. If unspecified, defaults to the wirebird()->model(). Setting value to "memory" will build a model based on a memory store.

=cut

sub model {
    my ( $self, $model ) = @_;

    if ($model) {
        if ( lc($model) eq 'memory' ) {
            $self->{'model'} = RDF::Trine::Model->temporary_model;
        }
        else {
            $self->{'model'} = $model;
        }
    }
    return $self->{'model'} || $self->wirebird->model();
}

=head2 status ( [$status] )

Sets or returns the http status code that currently applies to this
resource. Defaults to 501 (Not Implemented).

=cut

sub status {
    my ( $self, $status ) = @_;

    if ($status) {
        $self->{'status'} = $status;
    }
    $self->{'status'} ||= 501;
    return $self->{'status'};
}

=head2 warnings ( [$warning] )

Sets or returns the reflist of human-readable warnings that have
happened to this resource.

=cut

sub warnings {
    my ( $self, @warnings ) = @_;

    $self->{'warnings'} ||= [];
    if (@warnings) {
        push @{ $self->{'warnings'} }, @warnings;
    }
    return $self->{'warnings'};
}

=head2 errors ( [$error] )

As warnings(), but for things that prevent further action.

=cut

sub errors {
    my ( $self, @errors ) = @_;

    $self->{'errors'} ||= [];
    if (@errors) {
        push @{ $self->{'errors'} }, @errors;
    }
    return $self->{'errors'};
}

=head2 children( [$listref] )

Sets or returns the reference to a list of other Resource objects that
are in some way children of this object.

So far, just used for XML feeds to avoid re-parsing for each child
entry.

=cut

sub children {
    my ( $self, $children ) = @_;
    $self->{'children'} ||= [];
    if ($children) {
        $self->{'children'} = $children;
    }
    return $self->{'children'};
}

=head2 commit()

Adds the current model(), if any, to the wirebird->model() and clears the current model().

=cut

sub commit {
    my ( $self, $model ) = @_;
    $model ||= $self->{'model'};
    return if !$model;

    my $count = 0;
    foreach
      my $statement ( $model->get_statements( undef, undef, undef )->get_all )
    {
        # TODO catch errors?
        $self->wirebird->model->add_statement($statement);
        $count++;
    }

    delete $self->{'model'};
    return $count;
}

=head2 populate( $template, $url[, $data] )

Populates an HTML::Template object.

If the template includes nested TMPL_LOOPs, populate() will expand urls
by retrieving them from the default store() to whatever arbitrary depth
the template requires.

=cut

sub populate {
    my ( $self, $template ) = @_;

    my $data = $self->asHash;

    # $data may get changed, so maybe clone the reference someday?

    # set all the defaults; this might need pushed into the loop below
    $template->param( 'url',    $self->url );
    $template->param( 'domain', $self->wirebird->server );
    $template->param( 'image',  $self->wirebird->server . 'css/logo.png' );
    $template->param( 'json',
        $self->asJson )    #$self->wirebird->jsoncoder->encode($data) )
      if !$template->param('json');

    #$template->param( 'script',     [] );
    $template->param( 'breadcrumb', [] );
    $template->param( 'sitename',    'Wirebird' );
    $template->param( 'description', 'Wirebird' );
    $template->param(
        'name',
        join( ' | ',
            ( $data->{'sioc:name'} || $data->{'sioc:about'}, 'Wirebird' ) )
    );

    foreach my $parm ( $template->param ) {

        # skip already-populated parms, or those we have no data for
        next if $template->param($parm) || !exists $data->{$parm};

        if ( $template->query( name => $parm ) eq 'LOOP' ) {

            # loops get populated recursively
            $template->param( $parm,
                $self->_populoop( $template, [$parm], $data->{$parm} ) );
        }
        elsif ($template->query( name => $parm ) eq 'VAR'
            && reftype $data->{$parm}
            && reftype $data->{$parm} eq reftype [] )
        {
            $template->param( $parm, $data->{$parm}->[0] );
        }
        else {
            $template->param( $parm, $data->{$parm} );
        }
    }
    return $template;
}

=head2 populate( $template, $parm, $data )

An internal function called by populate() for recursive template loops.

=cut

sub _populoop {
    my $self     = shift;
    my $template = shift;
    my $parm     = shift;
    my $data     = shift;

    my $value = $data;

    if ( !reftype($data) ) {

        # single url, we hope
        $value = [
            Wirebird::Resource->new(
                wirebird => $self->wirebird,
                url      => $data
            )->asHash
        ];
    }
    elsif ( reftype($data) eq reftype [] ) {

        # array, possibly of just urls
        $value = [
            map {
                reftype $_
                  ? $_
                  : Wirebird::Resource->new(
                    wirebird => $self->wirebird,
                    url      => $_
                  )->asHash
              }
              sort @{$data}
        ];
    }
    foreach my $parm2 ( $template->query( loop => $parm ) ) {
        next if $parm2 =~ /^\_\_/x;    # internal Html::Template variables
                                       # populate any sub-loops
        if ( $template->query( name => [ @$parm, $parm2 ] ) eq 'LOOP' ) {

            # iterate over all the entries, annoyingly
            for ( my $i = 0 ; $i < scalar @$value ; $i++ ) {
                if ( !reftype( $value->[$i]->{$parm2} ) ) {

                    # single url, we hope
                    $value->[$i]->{$parm2} = [
                        Wirebird::Resource->new(
                            wirebird => $self->wirebird,
                            url      => $value->[$i]->{$parm2}
                        )->asHash
                    ];
                }
                elsif ( reftype( $value->[$i]->{$parm2} ) eq reftype [] ) {

                    # array, possibly of just urls
                    $value->[$i]->{$parm2} = [
                        map {
                            reftype $_ ? $_ : Wirebird::Resource->new(
                                wirebird => $self->wirebird,
                                url      => $_
                              )->asHash
                        } @{ $value->[$i]->{$parm2} }
                    ];
                }

                # recurse
                $value->[$i]->{$parm2} = $self->_populoop(
                    $template,
                    [ @$parm, $parm2 ],
                    $value->[$i]->{$parm2}
                );
            }
        }
    }
    return $value;
}

=head2 pageExists()

Returns true if one or more statements with the url as subject exist in
the store for the resource.

NB: Rules may mean that such statements can be inferred from other
statements with the url as object.

=cut

sub pageExists {
    my $self = shift;
    my $url = shift || $self->url;
    return if !$url;
    return $self->wirebird->model->count_statements( iri($url), undef, undef );
}

=head2 template( $tmplname )

Returns the HTML::Template structure by that name (no extension), or
the default template if none exists.

=cut

sub template {
    my $self = shift;
    my $tmplname = shift || $self->tmplName;
    if ( !-f ( 'views/' . $tmplname . '.tmpl' ) ) {
        ERROR "Missing $tmplname template";
        $tmplname = 'default';
    }
    return HTML::Template->new(
        filename          => 'views/' . $tmplname . '.tmpl',
        die_on_bad_params => 0,
        loop_context_vars => 1,
    );
}

=head2 tmplName( [$type] )

Returns the default template name (no extension) for this resource.
Defaults to no type (full page).

=cut

sub tmplName {
    my $self     = shift;
    my $tmplname = 'default';
    if ( exists $self->asHash->{'@type'} ) {
        $tmplname = lc(
            reftype $self->asHash->{'@type'}
            ? join( '_', sort @{ $self->asHash->{'@type'} } )
            : $self->asHash->{'@type'}
        );
    }
    return $tmplname;
}

=head2 asHtml($url[, $template])

Returns the populated template for the resource. If no HTML::Template
object is passed, calls template().

=cut

sub asHtml {
    my ( $self, $template, $page ) = @_;
    foreach my $handler (
        sort {
            { $self->identities }
            ->{$b} <=> { $self->identities }->{$a}
        } keys %{ $self->identities }
      )
    {
        if ( $handler->can('asHtml') ) {
            $template = $handler->asHtml( $self, $template, $page );
        }
    }
    $template ||= $self->template();

    $self->populate( $template, $self->url, $self->asHash );

    return $template;
}

=head2 asRdfXml()

Returns the RDF/XML data string.

=cut

sub asRdfXml {
    my $self = shift;

    #TODO namespaces!
    return RDF::Trine::Serializer->new('RDFXML')->serialize_iterator_to_string(
        $self->wirebird->model->get_statements(
            iri( $self->url ),
            undef, undef
        )
    );
}

=head2 asJson()

Returns the JSON-encoded data string.

=cut

sub asJson {
    my $self = shift;

    #return $self->wirebird->jsoncoder->encode( $self->asHash );
    return RDF::Trine::Serializer->new('RDFJSON')
      ->serialize_iterator_to_string(
        $self->wirebird->model->get_statements(
            iri( $self->url ),
            undef, undef
        )
      );
}

=head2 asTurtle()

Returns a string in Turtle format representing the resource.

=cut

sub asTurtle {
    my $self = shift;
    return RDF::Trine::Serializer->new('Turtle')->serialize_iterator_to_string(
        $self->wirebird->model->get_statements(
            iri( $self->url ),
            undef, undef
        )
    );
}

=head2 asHash()

Builds a pseudo-JSON hash from the default store. Namespaces are
compressed to prefixes and added to the attribute name.

If a hash is passed as the url, it will be assumed to be an
already-expanded url, and returned unchanged (and unchecked).

If the corresponding JSON file exists, it is loaded for speed. If it
doesn't exist, it's created during the process.

=cut

sub asHash {
    my $self = shift;

    return {} unless $self->url;

    my $json = {};
    my $iter =
      $self->wirebird->model->get_statements( iri( $self->url ), undef, undef );
    while ( my $row = $iter->next ) {
        my $nodes = [ $row->nodes ];
        my $q     = $self->wirebird->sn->qname( $nodes->[1]->value );
        if ( !$q ) {
            $q = $nodes->[1]->value;
            ERROR "No qname for $q\n";
        }
        if (
            ( $q eq 'rdf:type' )
            && ( my ( $c, $t ) =
                $self->wirebird->sn->qname( $nodes->[2]->value ) )
          )
        {
            if ( $json->{'@context'} ) {
                if ( !reftype $json->{'@context'} ) {
                    $json->{'@context'} = [ $json->{'@context'} ]
                      ;    # unless $1 eq $json->{'@context'};
                    $json->{'@type'} =
                      [ $json->{'@type'} ];    # unless $3 eq $json->{'@type'};
                }
                push @{ $json->{'@context'} }, $self->wirebird->ns->$c
                  unless match( $self->wirebird->ns->$c, $json->{'@context'} );
                push @{ $json->{'@type'} }, $t
                  unless match( $t, $json->{'@type'} );
            }
            else {
                $json->{'@context'} = $self->wirebird->ns->$c;
                $json->{'@type'}    = $t;
            }
        }
        else {
            if ( !exists $json->{$q} ) {
                $json->{$q} = _unify( $nodes->[2]->value );
            }
            elsif ( !reftype $json->{$q} ) {
                $json->{$q} = [ $json->{$q}, _unify( $nodes->[2]->value ) ];
            }
            elsif ( reftype $json->{$q} eq reftype [] ) {
                push @{ $json->{$q} }, _unify( $nodes->[2]->value );
            }
            else {
                ERROR "Something ain't right";
            }
        }
    }

    if ( scalar keys %$json == 0 ) {
        DEBUG 'Failed to load ', $self->url, " json somehow";
        return { 'url' => $self->url };
    }
    $json->{'url'} ||= $self->url;
    $self->{'data'} = $json;

    return $self->{'data'};
}

=head2 fromHash($hashref)

Given a hashref, will dump all the key/values to the permanent store
using the resource url.

Does not (yet?) preserve order on values that are arrays.

=cut

sub fromHash {
    my ( $self, $hashref ) = @_;
    foreach my $pair ( @{ _flatten($hashref) } ) {
        if ( URI->new( $pair->[1] )->has_recognized_scheme ) {
            $self->set( $pair->[0], iri( $pair->[1] ) );
        }
        else {
            $self->set( $pair->[0], literal( $pair->[1] ) );
        }
    }
    $self->rewrite();
    return $self;
}

sub _flatten {
    my $hashref = pop;
    my $array   = [];
    foreach my $key ( keys %$hashref ) {
        if ( ( reftype $hashref->{$key} || q{} ) eq reftype [] ) {
            foreach ( @{ $hashref->{$key} } ) {
                push @$array, [ $key, $_ ];
            }
        }
        else {
            push @$array, [ $key, $hashref->{$key} ];
        }
    }
    return $array;
}

=head2 create ($parms)

Use the resource as a factory to (probably) create a new resource.

Accepts an URL (deprecated) or a set of parameters which it will try to make sense of.

	model => RDF::Trine::Model->new...

	url => 'http:...'

	pairs => [body_parameters->flatten]

	headers => response->headerspairs

	uploads => response->uploads

INCOMPLETE

=cut

sub create {
    my $self = shift;
    my $parms;
    if ( scalar @_ == 1 ) {
        if ( reftype $_->[0] ) {
            $parms = shift;
        }
        elsif ( !reftype $_->[0] && URI->new( $_->[0] )->has_recognized_scheme )
        {
            $parms = { url => shift };
        }
        else {
            $parms = { model => shift };
        }
    }
    else {
        $parms = {@_};
    }
    print "Parms ", Dumper($parms), "\n";

    my $child;

    # is parm a nekkid url, or a simple {url=>$remoteurl} hash?
    if ( exists $parms->{'url'}
        && !$self->wirebird->isLocal( $parms->{'url'} ) )
    {
        $child = Wirebird::Resource->new(
            wirebird => $self->wirebird,
            url      => $parms->{'url'}
        );
        $child->retrieve();
        delete $parms->{'url'};
    }
    elsif ( blessed $parms ) {
        $child = $parms;
        $parms = {};
    }
    else {
        $child = Wirebird::Resource->new( wirebird => $self->wirebird );

        # parms better have something if we're here
    }

    my $bid = {};
    foreach my $handler ( $self->wirebird->handlers ) {
        if ( $handler->can('bidCreate') ) {
            my $priority = $handler->bidCreate( $self, $child, $parms );
            $bid->{$handler} = $priority if $priority;
        }
    }
    foreach my $handler ( sort { $bid->{$b} <=> $bid->{$a} } keys %$bid ) {
        last if $bid->{$handler} < 1;
        $handler->create( $self, $child, $parms );

# handlers should check $child->status to avoid unwanted duplication of handling, but everybody gets a shot at it.
    }
    if ( !$child || $child->status == 501 ) {
        DEBUG "No plugin handled this";
        DEBUG "Parent: ", $self->url;
        DEBUG join( ', ', keys %{ $self->identities } );
        DEBUG "Child: ", $child->url if $child;
        DEBUG join( ', ', keys %{ $child->identities } ) if $child;

        # TODO nothing handled it, so if it's a valid structure and
        # we're authz'd for this directory I guess we'll just store it.
        #if (some kind of check) {
        #	$child->store;
        #	$child->status(201);
        #}
    }

    return $child;
}

=head2 update ($data)

Replaces the resource with the passed data.

TODO: CURRENTLY UNFINISHED

=cut

sub update {
    my $self = shift;
    my $data = shift;
    my $bid  = {};

    ERROR('Called incomplete Resource->update');

    return $self;
}

=head2 del ()

Deletes the resource, and any pointers to it.

CURRENTLY UNFINISHED

=cut

sub del {
    my $self = shift;

    #TODO return $return unless $self->pageExists;
    $self->wirebird->model->add_statement(
        statement(
            iri( $self->url ),
            iri('http://www.w3.org/ns/activitystreams#deleted'),
            literal( strftime( '%Y-%m-%dT%H:%M:%SZ', gmtime() ) )
        )
    );
    my $oldtypes = $self->wirebird->query(
        'PREFIX as: <' . $self->wirebird->ns->as . '>
PREFIX rdf: <' . $self->wirebird->ns->rdf . '>
#INSERT { <' . $self->url . '> as:formerType :type . }
#DELETE { <' . $self->url . '> rdf:type :type . }
SELECT *
WHERE  { <' . $self->url . '> rdf:type :type . }'
    );
    foreach (@$oldtypes) {
        $self->wirebird->model->remove_statement(
            statement( iri( $self->url ), curie('rdf:type'), $_->{'type'} ) );
    }
    $self->wirebird->model->add_statement(
        statement(
            iri( $self->url ),
            curie('rdf:type'),
            iri('http://www.w3.org/ns/activitystreams#Tombstone')
        )
    );

    return $self;
}

=head2 hasType ( $url )

Returns true if the resource is this type.

=cut

sub hasType {
    my $self    = shift;
    my $typeurl = curie(shift);
    return $self->wirebird->model->get_statements( iri( $self->url ),
        curie('rdf:type'), $typeurl )->get_all;
}

=head2 identities()

Returns a hashref whose keys are all the Wirebird::Handler plugin
classes that assert a claim in handling resources of this type. Values
are the claim's priority.

=cut

sub identities {
    my $self = shift;
    if ( !exists $self->{identities} ) {
        $self->{identities} = {};
        foreach my $handler ( $self->wirebird->handlers ) {
            if ( $handler->can('priority') ) {
                my $priority = $handler->priority($self);
                $self->{identities}->{$handler} = $priority if $priority;
            }
        }
    }
    return $self->{identities};
}

=head2 changed ()

Removes static files so they will be regenerated when next requested.
Call when data store changes.

TODO: Name is ambiguous

=cut

sub changed {
    my $self = shift;
    return if $self->wirebird->isLocal( $self->url );
    foreach my $ext ( ( 'json', 'ttl', 'rdf', 'xml', 'html' ) ) {
        if ( -f ( 'static/' . $self->filename . '.' . $ext ) ) {
            unlink 'static/' . $self->filename . '.' . $ext;
        }
    }
    return 1;
}

=head2 retrieve ()

Populates the resource with the contents of the provided URL.

NB: $self->url may be set with a different URL depending on the results
(for instance, Wirebird::Remote::XMLFeed will use the base url instead
of the feed url, since the feed url may change in the future).

=cut

sub retrieve {
    my $self = shift;
    my $uri = URI->new( shift || $self->url );
    if ( !$uri->has_recognized_scheme ) {
        $self->errors("Cannot retrieve unrecognized uri $uri");
        return;
    }
    if ( $uri->scheme eq 'http' || $uri->scheme eq 'https' ) {
        my $header = HTTP::Headers->new;

#$header->header( 'Accept' => 'application/rdf+xml; text/rdf+n3; application/rdf+turtle; application/x-turtle; application/turtle; application/xml; application/activity+json; application/json; text/html; text/*; image/png; image/gif; image/jpeg; image/*; */*');
        $header->header( 'Accept-Charset' => 'utf-8' );
        my $request = HTTP::Request->new( 'GET', $uri->canonical, $header );
        my $response = $self->wirebird->userAgent->request($request);
        if ( !$response->is_success ) {
            $self->errors( $response->status_line );
            ERROR $response->status_line, " on ", $uri->canonical;
            $self->status(500);    # TODO maybe something more advanced here
            return;
        }

        # give each plugin a shot at the response
        my $bids = {};
        foreach my $pagetype ( $self->wirebird->pagetypes ) {
            next unless $pagetype->can('bid');
            $bids->{$pagetype} = $pagetype->bid($response);
        }

        # who liked it best?
        #~ my ($pagetype) = sort { $bids->{$b} <=> $bids->{$a} } keys %$bids;
        #~ if ( !$pagetype || $bids->{$pagetype} == 0 ) {
        #~ $self->errors( 'Could not identify remote page of type '
        #~ . $response->content_type );
        #~ ERROR 'Could not identify remote page of type '
        #~ . $response->content_type;
        #~ $self->status(501);    # TODO maybe something more advanced here
        #~ return;
        #~ }

        #bless $self, $pagetype;
        foreach
          my $pagetype ( sort { $bids->{$b} <=> $bids->{$a} } keys %$bids )
        {
            last if $bids->{$pagetype} <= 0;
            $self->commit( $pagetype->retrieve( $self, $response ) );

        }
    }
    elsif ( $uri->scheme eq 'mailto' ) {
        Wirebird::Remote::Mailto->retrieve( $self, $uri );
    }
    else {
        $self->errors( 'Cannot handle uri scheme ' . $uri->scheme . ' yet' );
    }
    return $self;
}

=head2 merge( $data )

Merges $data (an RDF::Trine::Model OR iterator) into this resource's store.

=cut

sub merge {
    my ( $self, $data ) = @_;
    ERROR 'DEPRECATED USE OF Resource->merge';

    # TODO remove existing stuff!
    if ( blessed($data) =~ /Model/ ) {
        $data = $data->get_statements( undef, undef, undef, undef );
    }
    while ( my $row = $data->next ) {
        push @{ $self->model }, $row;
    }
    return $data;
}

=head2 set( $pred, $obj )

Shortcut to set a triple with the resource url as subject. Object
should already be a valid RDF::Trine::Node, but predicate should be a
curie.

=cut

sub set {
    my ( $self, $pred, $obj ) = @_;
    return $self->wirebird->model->add_statement(
        statement( iri( $self->url ), curie($pred), $obj ) );
}

sub updateTriple {
    my ( $self, $pred, $obj ) = @_;
    my $url = $self->url;
    $pred = curie($pred)->as_string;
    $self->query(
        'SELECT :oldobj
DELETE { <$url> <$pred> :oldobj . }
INSERT { <$url> <$pred> $obj . }
WHERE  { <$url> <$pred> :oldobj . }'
    );
}

=head2 httpHeaders()

Returns a hashref with the (non-exhaustive) headers to be served with the
resource. Includes Link, Allow, and Accept-VERB.

=cut

sub httpHeaders {
    my ($self) = @_;
    my $header = {};

    # TODO: do we really need all of these in the header, or just LDP ones?
    $header->{'Link'} = join(
        ', ',
        map { $_->[2]->as_string . '; rel="type"' }
          $self->wirebird->model->get_statements( iri( $self->url ),
            curie('rdf:type'), undef )->get_all
    );

    # TODO: add as appropriate:
    # <http://www.w3.org/ns/ldp#Resource>; rel="type"
    # <http://www.w3.org/ns/ldp#BasicContainer>; rel="type"
    # TODO call plugins and acl's for this!
    $header->{'Allow'}       = 'GET,POST,PUT,DELETE';
    $header->{'Accept-Post'} = 'text/turtle, application/ld+json'
      if $header->{'Allow'} =~ /POST/x;
    $header->{'Accept-Patch'} = 'text/ldpatch'
      if $header->{'Allow'} =~ /PATCH/x;

    # eTag?
    return $header;
}

=head2 rewrite()

Deletes and recreates the static versions of the resource (by calling change()
and then write()).

=cut

sub rewrite {
    my ($self) = @_;
    return if $self->wirebird->isLocal( $self->url );
    $self->change();
    return $self->write();
}

=head2 write()

Creates the static versions (html, ttl, json, and rdf) of the resource iff they
don't exist.

=cut

sub write {
    my ($self) = @_;
    return if $self->wirebird->isLocal( $self->url );
    path( 'static/' . $self->filename )->parent->mkpath;

    if ( !-f ( 'static/' . $self->filename . '.html' ) ) {
        open my $FH, '>', 'static/' . $self->filename . '.html' or croak $!;
        print $FH $self->asTurtle;
        close $FH;
    }

    if ( !-f ( 'static/' . $self->filename . '.ttl' ) ) {
        open my $FH, '>', 'static/' . $self->filename . '.ttl' or croak $!;
        print $FH $self->asTurtle;
        close $FH;
    }

    if ( !-f ( 'static/' . $self->filename . '.json' ) ) {
        open my $FH, '>', 'static/' . $self->filename . '.json' or croak $!;
        print $FH $self->asJson;
        close $FH;
    }

    if ( !-f ( 'static/' . $self->filename . '.rdf' ) ) {
        open my $FH, '>', 'static/' . $self->filename . '.rdf' or croak $!;
        print $FH $self->asRdfXml;
        close $FH;
    }
    return 1;
}

sub DESTROY {
    my $self = shift;
    if ( !$self->pageExists ) {
        DEBUG "Destroying ", $self->url;
    }
    return 1;
}

sub _unify {
    return decode( "UTF-8", shift );
}

1;
