# Wirebird

**Check the [Wirebird Blog](http://wirebird.com/archive.html) for current info.**

Wirebird is (or will be) a lightweight, user-friendly federated social 
media client/platform, written in Perl 5.

## Purpose

Wirebird provides a standards-based (RESTful, structured/linked data; 
ActivityPub, etc.) way to read and publish social media.

## Project status

Incomplete. So very incomplete.

## Setup

### Installation

You probably don't want to do that yet. But if you do, it just runs 
from its source directory. You're on your own for dependency 
installation until things settle down a little.

### Configuration

You'll either want to create a Postgres database named "wirebird" or 
edit the main library to fall back to SQLite.

## Usage

From the main Wirebird directory, run script/Wirebird.pl and that's it. 
Connect to the Dancer2 server at the address it tells you. It will have 
created a user based on the user who first runs the script. That's it. 
It doesn't do much yet.
