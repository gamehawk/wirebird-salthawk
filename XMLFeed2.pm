package Wirebird::Remote::XMLFeed;

use strict;
use warnings;

use XML::Feed;
$XML::Atom::ForceUnicode = 1;
use RDF::Trine::Model;
use RDF::TrineX::Functions -all;
use URI;
use Encode;
use HTML::FormatMarkdown;

use Data::Dumper;

=head2 bid ($response)

Returns priority claim of 100 if response's content can be successfully
parsed as an XML::Feed.

=cut

#~ sub bid {
	#~ my ( $class, $response ) = @_;

	#~ if ( $response->content_type =~ /xml/ ) {

		#~ # TODO what an ugly way to cache this
		#~ $response->{'_wb_asfeed'} ||=
		  #~ XML::Feed->parse( \join( "\n", $response->decoded_content ) );
		#~ return 100
		  #~ if ( $response->{'_wb_asfeed'}
			#~ && $response->{'_wb_asfeed'}->format eq 'Atom' );
		#~ return 99 if $response->{'_wb_asfeed'};
	#~ }
	#~ elsif ( $response->content_type =~ /html/ ) {
		#~ my @feeds = XML::Feed->find_feeds( $response->base );
		#~ if (@feeds) {
			#~ # TODO maybe prioritize feeds instead of taking first one?
			#~ $response->{'_wb_asfeed'} ||=
			  #~ XML::Feed->parse( URI->new( $feeds[0] ) );
			#~ if (! $response->{'_wb_asfeed'}) {
				#~ return 0;
			#~ }
			#~ #if ($response->{'_wb_asfeed'}) {
				my $link = XML::Atom::Link->new;
				$link->type('application/xml');
				$link->rel('self');
				$link->href($feeds[0]);
				$response->{'_wb_asfeed'}->self_link($link);
				#~ $response->{'_wb_asfeed'}->self_link($feeds[0]);
			#~ #}
#~ #			print Dumper ($response->{'_wb_asfeed'}->self_link()), "\n";
				#~ return 100 if $response->{'_wb_asfeed'}->format eq 'Atom' ;
				#~ return 99;
		#~ }
	#~ }

	#~ return 0;
#~ }

=head2 retrieve ($resource, $response)

Parses the response as an XML feed and sets the resource's triples
accordingly. Also sets the resource->url to the feed's base - the
current feed url is stored in the sioc:feed proprity, but since that
may change if the target site changes software it's the feed's base
that is considered canonical.

Currently sets the resource to a sioc:WebLog, but may take a closer
look in the future to see what kind of feed it really is.

Does *not* parse the feed's entries, only the feed information. Use
pollsubscription() after retrieve() to process the entries.

=cut

sub retrieve {
	my $class    = shift;
	my $resource = shift;
	my $response = shift;

	# TODO cached by bid();
	my $feed;
	my $feedurl;
	if (exists $response->{'_wb_asfeed'}) {
		$feed = $response->{'_wb_asfeed'};
		$feedurl = $response->{'_wb_asfeed'}->self_link();
		if (ref $feedurl) {
			$feedurl = $feedurl->{'href'};
		}
		print "Feedurl $feedurl\n";
	}
	else {
		$feed = XML::Feed->parse( \join( "\n", $response->decoded_content ) );
		$feedurl = $response->base;
		print "Feedurl2 $feedurl\n";
	}

	$resource->url( $feed->link );
	if ( $resource->pageExists ) {
		$resource->status(200);
		return $resource;
	}

	#TODO better response code
	$resource->status( $response->code );
	$resource->model->add_statement(
		statement(
			iri( $feed->link ), curie('rdf:type'), curie('sioc:WebLog')
		)
	);
	my $title = _unify( $feed->title );
	print "Title: $title in $feedurl\n";
	if (Encode::is_utf8($title)) {
		print "Title is utf8\n";
	}

	# TODO fix encoding as necessary
	$resource->model->add_statement(
		statement( iri( $feed->link ), curie('sioc:name'), literal($title) ) );
	$resource->model->add_statement(
		statement(
			iri( $feed->link ),
			curie('sioc:feed'),
			iri( $feedurl )
		)
	);
	$resource->model->add_statement(
		statement( iri( $feed->link ), curie('sioc:link'), iri( $feed->link ) )
	);
	my $author;

	if ( $feed->format eq 'Atom' ) {
		my $atom = $feed->{atom};    # ugly but XML::Feed just gives us the name
		if ( !$atom->author ) {

			# yes we have no author today
			# TODO (maybe pull the base page and look further someday)
			return $resource;
		}

		# TODO even XML::Atom doesn't surface everything that's there
		my $authoremail = $atom->author->email;
		my $authoruri =
		  URI->new( $atom->author->uri || ( 'mailto:' . $authoremail ) );
		my $authorname = _unify($atom->author->name);
		print "Authorname: $authorname in ", $feed->self_link, "\n";
		# TODO fix encoding as necessary

		$author = Wirebird::Resource->new(
			wirebird => $resource->wirebird,
			url      => $authoruri
		)->retrieve;
		if ( !$author->pageExists ) {
			$author->model->add_statement(
				statement(
					iri( $author->url ), curie('sioc:name'),
					literal($authorname)
				)
			) if $authorname;
			$resource->merge( $author->model );
		}
		$resource->model->add_statement(
			statement(
				iri( $feed->link ),
				curie('sioc:has_creator'),
				iri( $author->url )
			)
		);
	}
	elsif ( $feed->format =~ /RSS/i ) {

		# XML::RSS returns stuff not encoded. WHYYYY
		if (
			   $feed->{rss}
			&& $feed->{rss}->channel
			&& (   $feed->{rss}->channel('webMaster')
				|| $feed->{rss}->channel->{dc}{creator} )
		  )
		{
			$author = $feed->{rss}->channel('webMaster')
			  || $feed->{rss}->channel->{dc}{creator};
		}
		else {
			print "unable to get author from xml::feed\n";
		}
	}
	else {
		print "unable to parse feed\n";
	}

	#~ $resource->model->add_statement(statement(
	#~ iri($feed->link),
	#~ curie('sioc:attributed_to'),
	#~ iri($author->url)
	#~ )) if $author;
	return $resource;
}

=head2 getSubscribers( $resource )

Returns all the inbox resources for subscribers to the resource
(currently only explicit has_subscriber properties, but eventually will
use a proper SPARQL search).

=cut

sub getSubscribers {
	my $class    = shift;
	my $resource = shift;
	my $success  = 0;

	my @inboxes = map {
		Wirebird::Resource->new(
			wirebird => $resource->wirebird,
			url      => $_->[2]->value
		  )
	  }
	  map {
		$resource->wirebird->model->get_statements( iri( $_->[2] ),
			curie('as:inbox'), undef )->get_all
	  } $resource->model->get_statements( undef, curie('sioc:has_subscriber'),
		undef )->get_all;
	return \@inboxes;
}

=head2 pollSubscription( $resource[, $inboxes])

If the resource has any , retrieves the current feed(s) for the resource, iterates over
the entries, and adds any new ones to the store and links them into
subscribers' inboxes (if provided; an arrayref of Wirebird::Resources).

Currently builds the entry resources from the XML::Feed::Entry, but
maybe someday will attempt its own Wirebird::Remote retrieval of the
actual pages.

=cut

sub pollSubscription {
	my ($class, $resource, $inboxes)    = @_;

	$inboxes ||= [];
	my $success  = 0;
print "Inboxes ", join(', ', @$inboxes), "\n";
	my @feeds =
	  map { $_->[2]->value }
	  $resource->model->get_statements( undef, curie('sioc:feed'), undef )
	  ->get_all;    # no subj because we're in this resource's model
	                # TODO sort feed by priority (Atom first, etc.)
	foreach my $feedurl ( sort @feeds ) {
		my $feed = XML::Feed->parse( URI->new($feedurl) );
		if ( !$feed ) {
			print "oops, could not parse $feedurl\n";

			# TODO report an error, try to fix it?
			next;
		}
		$success++;

		# update the update time
		my ($stamp) =
		  map { $_->[2]->value }
		  $resource->model->get_statements( undef,
			curie('sioc:last_item_date'), undef )->get_all;
		$resource->wirebird->model->remove_statements( iri( $resource->url ),
			curie('sioc:last_item_date'), undef );

#		$resource->wirebird->model->add_statement(statement(iri($resource->url), curie('sioc:last_item_date'), literal(DateTime->now()->iso8601() . 'Z')));

		foreach my $entry ( sort { $a->issued->epoch() <=> $b->issued->epoch() }
			$feed->entries )
		{
			# TODO do a full retrieval instead of parsing the feed?
			my $entresource = Wirebird::Resource->new(
				wirebird => $resource->wirebird,
				url      => $entry->link
			);
			if ($feed->format eq 'Atom') {
				$class->atomEntry($feed->{atom}, $entry->{entry}, $entresource);
			}

			if (
				!$entresource->model->count_statements(
					iri( $entresource->url ),
					curie('sioc:content'), undef
				)
			  )
			{
				$entresource->model->add_statement(
					statement(
						iri( $entresource->url ),
						curie('sioc:content'),
						literal(
							HTML::FormatMarkdown->format_string(
								_unify(
									$entry->content->body || $entry->summary
								)
							)
						)
					)
				);
			}
			if ($author) {
				$entresource->model->add_statement(
					statement(
						iri( $entresource->url ),
						curie('sioc:has_creator'),
						iri( $author->url )
					)
				);
			}
			else {
				($author) =
				  map { $_->[2] }
				  $resource->model->get_statements( undef,
					curie('sioc:has_creator'), undef )->get_all;
				$entresource->model->add_statement(
					statement(
						iri( $entresource->url ), curie('sioc:has_creator'),
						iri($author)
					)
				) if $author;
			}

			$entresource->wirebird->merge( $entresource->model );
			$entresource->changed;
			$resource->wirebird->model->add_statement(
				statement(
					iri( $resource->url ), curie('sioc:last_item_date'),
					literal($stamp)
				)
			);
			foreach my $inbox (@$inboxes)
			{
				$inbox->create($entresource);
			}
		}
	}
	return $success;
}

sub atomEntry {
	my ($class, $feed, $entry, $resource) = @_;

	$resource->retrieve();

	# TODO fixup author
	my $author;

	if (
		   $entry->author
		&& (   $entry->author->uri
			|| $entry->author->email )
	  )
	{
		if (  !$entry->author->uri
			|| $entry->author->uri eq
			$entry->author->email )
		{
			$entry->author->uri(
				'mailto:' . $entry->author->email );
			($author) =
			  map { $_->[0]->value }
			  $resource->wirebird->model->get_statements( undef, undef,
				iri( $entry->author->uri ) )->get_all;
		}
		$author = Wirebird::Resource->new(
			wirebird => $resource->wirebird,
			url      => (
					 $author
				  || $entry->author->uri
				  || $entry->author->email
			)
		);
		if ( !$author->pageExists ) {
			$author->retrieve;

	# TODO only set this if the author doesn't already exist/have a name
	#~ $author->model->add_statement(statement(
	#~ iri($feed->link),
	#~ curie('sioc:name'),
	#~ literal($entry->{entry}->author->name)
	#~ )) if $entry->{entry}->author->name;
			$resource->merge( $author->model );
		}
	}
	if ( !$entry->issued ) {
		$entry->issued( DateTime->from_epoch( epoch => time() ) );
	}
	if ( !$entry->modified ) {
		$entry->modified( DateTime->from_epoch( epoch => time() ) );
	}
	my $issued   = $entry->issued->datetime() . 'Z';
	my $modified = $entry->modified->datetime() . 'Z';

	if ( !$stamp || $issued gt $stamp ) {
		$stamp = $issued;
	}
	if ( !$stamp || $modified gt $stamp ) {
		$stamp = $modified;
	}

	# TODO don't stomp pre-existing data
	#~ $entresource->fromHash(
	#~ {
	#~ 'dct:created'  => $issued,
	#~ 'dct:modified' => $modified,
	#~ }
	#~ );
	$resource->model->add_statement(
		statement(
			iri( $entresource->url ), curie('rdf:type'),
			curie('sioc:BlogPost')
		)
	);
	$resource->model->add_statement(
		statement(
			iri( $entresource->url ), curie('sioc:has_container'),
			iri($feedurl)
		)
	);
	if (
		!$resource->model->count_statements(
			iri( $resource->url ), curie('content:encoded'),
			undef
		)
	  )
	{
		$resource->model->add_statement(
			statement(
				iri( $resource->url ),
				curie('content:encoded'),
				literal(
					XML::LibXML::CDATASection->new(
						$entry->content->body || $entry->summary
					)->toString
				)
			)
		);
	}
	return $resource;
}

sub rssEntry {
}

sub _unify {
	my $string = shift;
	if ( !Encode::is_utf8($string) ) {
		return decode( "UTF-8", $string );
	}
	return $string;
}

1;
