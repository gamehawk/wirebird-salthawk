#!/usr/bin/perl

use Dancer2;

use local::lib;
use lib( $ENV{'WIREBIRDWD'} || $ENV{'PWD'} )
  . "/lib";    # use local-directory libraries first

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($ERROR);
use Net::Domain qw(hostname);
use HTML::Template;

# TODO: put this in a config
set server         => hostname;
set port           => 6809;
# TODO re-enable this after middleware handles headers
#set public_dir     => path( app->location, 'static' );
#set static_handler => true;

use Wirebird;
my $wirebird = Wirebird->new();
use Wirebird::Resource;

# I feel kinda bad not using all of Dancer's routing, but everything
# gets handled based on its context type; URL structure can be somewhat
# dynamic. Dancer is probably not the best choice and we'll probably
# back off to generic Plack or something.

get '**' => sub {
	my ($mega) = splat;

	# todo authorization
	my $file = join( '/', @$mega );
	my $ext = 'html';
	if ( $file !~ s/\.(\w+)$// ) {
		if ( $file =~ /\/$/ ) {
			$file .= 'index';
		}
	}
	else {
		$ext = $1;
	}
    # TODO re-enable this and static_handler above eventually
	# if ( -f 'static/' . $file . '.' . $ext ) {
	# 	# catch static files that match after canonicalizing url
	# 	return send_file $file . '.' . $ext;
	# }
    my $page = query_parameters->get('page') || 1;
	my $resource = Wirebird::Resource->new( wirebird=>$wirebird, url => $wirebird->server . $file );
	if ( !$resource->pageExists ) {
		status 'not_found';
		template 'special_404', { path => request->path };
	}
	else {

        my $headers = $resource->httpHeaders();
        foreach my $headname (keys %$headers) {
            header( $headname, $headers->{$headname});
        }
        if ( $ext eq 'rdf' ) {
            header( 'Content-Type'  => 'application/rdf+xml' );
			return  $resource->asRdfXml();
        }
        if ( $ext eq 'ttl' ) {
            header( 'Content-Type'  => 'text/turtle' );
			return $resource->asTurtle();
        }
		if ( $ext eq 'json' ) {
            header( 'Content-Type'  => 'application/json' );
			return $resource->asJson();
		}
		return $resource->asHtml(undef, $page)->output;
	}
};

put '**' => sub {
	my ($mega) = splat;

	# todo authentication
	my $file = join( '/', @$mega );
	my $ext = 'html';
	if ( $file !~ s/\.(\w+)$// ) {
		if ( $file =~ /\/$/ ) {
			$file .= 'index';
		}
	}
	else {
		$ext = $1;
	}

	# todo authorization
	# todo validation
	# todo context
	my $resource = Wirebird::Resource->new( wirebird=>$wirebird, url => $wirebird->server . $file );

	if ($resource->pageExists) {
		$resource->update(model => param2model($resource->url), pairs => [body_parameters->flatten], headers => response->headers, uploads => response->uploads);
	}
	else {
		# TODO make sure we own this directory
		$resource->create(model => param2model($resource->url), pairs => [body_parameters->flatten], headers => response->headers, uploads => response->uploads);
	}


	# TODO update status, return warnings/errors, etc.!
    # http://www.w3.org/ns/ldp#constrainedBy ?
	if ( !$resource->pageExists ) {
		status 'not_found';
		template 'special_404', { path => request->path };
	}
	else {
		if ( $ext eq 'json' ) {

			# TODO actually return as json instead of HTML someday
			return $resource->asJson();
		}
		else {
			return $resource->asHtml()->output;
		}
	}
};

post '**' => sub {
	my ($mega) = splat;

	# todo authentication
	my $file = join( '/', @$mega );
	if (uc(query_parameters->{'_method'}) eq 'DELETE') {
		print "forwarding to POST\n";
		forward '/' . $file, { }, { method => 'DELETE' };
	}
	my $ext = 'html';
	if ( $file !~ s/\.(\w+)$// ) {
		if ( $file =~ /\/$/ ) {
			$file .= 'index';
		}
	}
	else {
		$ext = $1;
	}
	my $resource = Wirebird::Resource->new( wirebird=>$wirebird, url => $wirebird->server . $file );

	# todo authorization
	# todo validation
	# todo context

	my $child = $resource->create(model => param2model($resource->url), pairs => [body_parameters->flatten], headers => response->headers, uploads => response->uploads);

	# TODO update status, return warnings/errors, etc.!
	if ( !$resource->pageExists ) {
		status 'not_found';
		template 'special_404', { path => request->path };
	}
	else {
		if ( $ext eq 'json' ) {

			# TODO actually return as json instead of HTML someday
			return $resource->asJson();
		}
		else {
			return $resource->asHtml()->output;
		}
	}
};

del '**' => sub {
	my ($mega) = splat;


	# todo authentication
	my $file = join( '/', @$mega );
	my $ext = 'html';
	if ( $file !~ s/\.(\w+)$// ) {
		if ( $file =~ /\/$/ ) {
			$file .= 'index';
		}
	}
	else {
		$ext = $1;
	}
	my $resource = Wirebird::Resource->new( wirebird=>$wirebird, url => $wirebird->server . $file );
	if ( !$resource->pageExists ) {
		status 'not_found';
		template 'special_404', { path => request->path };
	}
	else {
		$resource->del;
		return $resource->asHtml()->output;
	}
};


any qr{.*} => sub {
	status 'not_found';
	template 'special_404', { path => request->path };
};

=head2 param2model( $url )

Builds an RDF::Trine::Model from the response.

Deprecated in favor of Turtle.

=cut

sub param2model {
    my $url = shift;
    my $model = [];
    foreach my $pair (body_parameters->flatten) {
        if (URI->new($pair->[1])->has_recognized_scheme) {
            push @$model, [iri($url), curie($pair->[0]), iri($pair->[1])];
        } else {
            push @$model, [iri($url), curie($pair->[0]), literal($pair->[1])];
        }
    }
    if (response->header('Link')) {
        foreach my $link (response->header('Link')) {
            my ($parsedlink) = HTTP::Link->parse($link);
            if ($parsedlink->{'relation'} eq 'type') {
                push @$model, [iri($url), curie('rdf:type'), iri($parsedlink->{'iri'})];
            }
            else {
                DEBUG('Link in header: ', $parsedlink->{'relation'}, ' = ', $parsedlink->{'iri'});
            }
        }
    }
    if (response->header('Slug')) {

    }
    return $model;
}

dance;

#~ Dancer2::Core::Error->new(
#~ response => response(),
#~ status   => 406,
#~ message  => "nothing but coal for you, I'm afraid",
#~ template => 'naughty/index',
#~ )->throw unless user_was_nice();
