#!/usr/bin/perl
# various temporary/test bits

use strict;
use Data::Dumper;
use Encode;

use RDF::Trine;
use RDF::Query;

my $store = RDF::Trine::Store->new(
		{
			storetype => 'DBI',
			name      => 'wb_main',
			#dsn      => "dbi:Pg:dbname=wirebird",
			dsn => "dbi:SQLite:dbname=data/wbstore.db",
			username => '',
			password => '',
		}
	);

my $model = RDF::Trine::Model->new( $store );

# Inbox Query
# my $query = RDF::Query->new( 'PREFIX sioc: <http://rdfs.org/sioc/ns#>
# PREFIX stream: <http://www.w3.org/ns/activitystreams#>
# PREFIX atom: <http://www.w3.org/2005/Atom/>
#
# SELECT *
# WHERE {
#  ?inbox sioc:has_owner <http://habrok:6809/users/silver> .
#  ?inbox stream:item ?item .
#  ?item stream:object ?remote .
#  ?remote atom:id ?url .
#  ?remote atom:title ?title .
#  ?remote atom:published ?published .
#  ?remote atom:content ?content .
# }' ) or die $!;
#~ # Post Query
#~ my $query = RDF::Query->new(
#~ 'PREFIX sioc: <http://rdfs.org/sioc/ns#>
#~ PREFIX stream: <http://www.w3.org/ns/activitystreams#>
#~ PREFIX atom: <http://www.w3.org/2005/Atom/>
#~ PREFIX dct: <http://purl.org/dc/terms/>
#~ PREFIX og: <http://ogp.me/ns#>
#~ PREFIX rel: <http://purl.org/vocab/relationship/>

#~ SELECT *
#~ WHERE {
 #~ <http://habrok:6809/users/silver/inbox/00000> stream:object ?remote .

 #~ ?remote atom:id ?url ;
  #~ atom:title ?name ;
  #~ atom:published ?published ;
  #~ atom:content ?content ;
  #~ dct:isPartOf ?parent .

 #~ OPTIONAL { ?remote rel:enclosure ?attachment . }
 #~ OPTIONAL {
  #~ ?remote atom:author ?attributedTo .
  #~ ?parent atom:author ?attributedTo .
 #~ }
 #~ OPTIONAL { ?remote og:image ?image . }

#~ }' ) or die $!;
# my $query = RDF::Query->new(
# 'PREFIX sioc: <http://rdfs.org/sioc/ns#>
# PREFIX stream: <http://www.w3.org/ns/activitystreams#>
# PREFIX atom: <http://www.w3.org/2005/Atom/>
# PREFIX dct: <http://purl.org/dc/terms/>
# PREFIX og: <http://ogp.me/ns#>
# PREFIX rel: <http://purl.org/vocab/relationship/>
#
# SELECT *
# WHERE {
#  <http://habrok:6809/users/silver/inbox> sioc:has_owner ?url .
#
#  ?url sioc:name ?name .
#  OPTIONAL { ?url dct:created ?created . }
#  OPTIONAL { ?url sioc:about ?about . }
#  OPTIONAL { ?url sioc:avatar ?avatar . }
# }' ) or die $!;

# my $query = RDF::Query->new(
# 'PREFIX sioc: <http://rdfs.org/sioc/ns#>
# PREFIX stream: <http://www.w3.org/ns/activitystreams#>
# PREFIX atom: <http://www.w3.org/2005/Atom/>
# PREFIX dct: <http://purl.org/dc/terms/>
# PREFIX og: <http://ogp.me/ns#>
# PREFIX rel: <http://purl.org/vocab/relationship/>
# PREFIX rdf: <https://www.w3.org/TR/rdf-schema/>
# SELECT *
# WHERE {
# 	<http://habrok:6809/users/silver/inbox> stream:item/rdf:rest* ?first .
# 	?first rdf:first ?item .
# 	?item stream:object ?url .
# 	?url atom:title ?name ;
# 		atom:published ?published ;
# 		atom:content ?content ;
# 		dct:isPartOf ?parent .
# 	OPTIONAL { ?url rel:enclosure ?attachment . }
# 	OPTIONAL { ?parent atom:author ?attributedTo . }
# 	OPTIONAL { ?parent atom:author/atom:name ?attributedToName . }
# 	OPTIONAL { ?url og:image ?image . } }
# 	ORDER BY ?published
# 	limit 20 offset 0' ) or die $!;

my $query = RDF::Query->new(
'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT *
WHERE {
	?wrapper dct:isPartOf <http://roc:6809/users/silver/inbox> .
	?tail rdf:first ?wrapper ;
	       rdf:rest rdf:nil .
}' ) or die $!;

my $iterator = $query->execute( $model );
while (my $row = $iterator->next) {
	foreach my $key (sort keys %$row) {
		print $key, ': ', $row->{$key}, "\n";
	}
	print "\n";
}

__END__
 streams:note (:object)
 	attachment
	attributedTo
	audience
	content
	context
	name
	endTime
	generator
	icon
	image
	inReplyTo
	location
	preview
	published
	replies
	startTime
	summary
	tag
	updated
	url
	to
	bto
	cc
	bcc
	mediaType
	duration

<http://wirebird.com/2018-06-18-not-much-change.html>   <http://purl.org/dc/terms/isPartOf>     <http://wirebird.com/atom.xml>
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://purl.org/vocab/relationship/self>       <http://wirebird.com/2018-06-18-not-much-change.html>
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://rdfs.org/sioc/ns#feed>  <atom.xml>
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://www.w3.org/2005/Atom/content>   "..."
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://www.w3.org/2005/Atom/id>        <http://wirebird.com/2018-06-18-not-much-change.html>
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://www.w3.org/2005/Atom/published> "2018-06-18T21:52:12-04:00"
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://www.w3.org/2005/Atom/title>     "Not much change"
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://www.w3.org/2005/Atom/updated>   "2018-06-18T21:52:12-04:00"
<http://wirebird.com/2018-06-18-not-much-change.html>   <http://www.w3.org/ns/activitystreams#url>      <http://wirebird.com/2018-06-18-not-much-change.html>
<http://wirebird.com/atom.xml>  <http://rdfs.org/sioc/ns#has_container> <http://habrok:6809/users/silver/subscriptionlist>
<http://wirebird.com/atom.xml>  <http://rdfs.org/sioc/ns#has_subscriber>        <http://habrok:6809/users/silver>
<http://wirebird.com/atom.xml>  <http://www.w3.org/2005/Atom/author>    <mailto:silver@phoenyx.net>
<http://wirebird.com/>  <http://purl.org/vocab/relationship/self>       <http://wirebird.com/>
<http://wirebird.com/>  <http://purl.org/vocab/relationship/self>       <http://wirebird.com//atom.xml>
<http://wirebird.com/>  <http://www.w3.org/2005/Atom/generator> <https://github.com/jmacdotorg/plerd>
<http://wirebird.com/>  <http://www.w3.org/2005/Atom/generator> "Plerd"
<http://wirebird.com/>  <http://www.w3.org/2005/Atom/id>        <http://wirebird.com/>
<http://wirebird.com/>  <http://www.w3.org/2005/Atom/title>     "Wirebird"
<http://wirebird.com/>  <http://www.w3.org/2005/Atom/updated>   "2018-06-18T21:52:15-04:00"
<mailto:silver@phoenyx.net>     <http://www.w3.org/2005/Atom/email>     "silver@phoenyx.net"
<mailto:silver@phoenyx.net>     <http://www.w3.org/2005/Atom/name>      "Karen"
